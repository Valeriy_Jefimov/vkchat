//
//  AppDelegate.h
//  VKChat
//
//  Created by Valery Efimov on 13.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <VKSdk.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

