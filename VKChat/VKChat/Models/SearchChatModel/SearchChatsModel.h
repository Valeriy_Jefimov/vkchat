//
//  SearchChatsModel.h
//  VKChat
//
//  Created by Valery Efimov on 02.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SearchChatsModel : NSObject



@property(strong,nonatomic) NSString *type;
@property(strong,nonatomic) UIImage  *avatarImage;
@property(strong,nonatomic) NSString *title;
@property(strong,nonatomic) NSString *photo50;
@property(strong,nonatomic) NSString *photo100;
//chat
@property(strong,nonatomic) NSString *adminId;
@property(strong,nonatomic) NSArray  *users;
@property(strong,nonatomic) NSString *chatID;
//user
@property(strong,nonatomic) NSString *userID;
@property(strong,nonatomic) NSString *firstName;
@property(strong,nonatomic) NSString *lastName;

- (instancetype)initWithJson:(NSDictionary *)json;

@end
