//
//  SearchChatsModel.m
//  VKChat
//
//  Created by Valery Efimov on 02.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "SearchChatsModel.h"


static NSString * const kMessage =             @"message";


@implementation SearchChatsModel


-(instancetype)initWithJson:(NSDictionary *)json  {
    self = [super init];
    
    if (self)
    { 
        self.users = [NSMutableArray new];
        [self baseSetupWithJson:json];
    }
    return self;
}

- (void)baseSetupWithJson:(NSDictionary *)json {

    self.title = NSLocalizedString(json[@"title"],nil);
    self.adminId = json[@"admin_id"];
    self.chatID = json[@"chat_id"];
    self.photo100 = json[@"photo_100"];
    self.userID = json[@"id"];
    self.photo50 = json[@"photo_50"];
    self.firstName = NSLocalizedString(json[@"first_name"],nil);
    self.lastName = NSLocalizedString(json[@"last_name"],nil);
    self.type = json[@"type"];
    self.users = json[@"users"];
    if(self.title.length < 2)
    {
        NSString *title = [NSString stringWithFormat:@"%@ %@",self.firstName,self.lastName];
        self.title = NSLocalizedString(title,nil);
    }
}



@end
