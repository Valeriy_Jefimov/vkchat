//
//  AttachDBModel+CoreDataProperties.h
//  VKChat
//
//  Created by Efimov on 05.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "AttachDBModel+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AttachDBModel (CoreDataProperties)

+ (NSFetchRequest<AttachDBModel *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *accessKey;
@property (nullable, nonatomic, copy) NSString *downloadUrl;
@property (nullable, nonatomic, copy) NSString *ident;
@property (nullable, nonatomic, copy) NSString *ownerId;
@property (nullable, nonatomic, copy) NSString *phototUrlSmall;
@property (nullable, nonatomic, copy) NSString *photoUrlLarge;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, copy) NSString *videoPlayerUrl;
@property (nullable, nonatomic, retain) MessageDBModel *message;

@end

NS_ASSUME_NONNULL_END
