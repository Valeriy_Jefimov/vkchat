//

//  AttachDBModel+CoreDataClass.m

//  VKChat

//

//  Created by Valery Efimov on 20.01.17.

//  Copyright © 2017 Valery Efimov. All rights reserved.

//



#import "AttachDBModel+CoreDataClass.h"

#import "MessageDBModel+CoreDataClass.h"

#import "VKManagerConstants.h"

#import "DataBaseManager.h"



#define CASE(str)                       if ([__s__ isEqualToString:(str)])

#define SWITCH(s)                       for (NSString *__s__ = (s); ; )

#define DEFAULT





@implementation AttachDBModel





+ (instancetype)newEntityWithContext:(NSManagedObjectContext*)context {
    
    NSEntityDescription *discr = [NSEntityDescription entityForName:NSStringFromClass([self class]) inManagedObjectContext:context];
    
    AttachDBModel * newAttach = [[AttachDBModel alloc] initWithEntity:discr
                                       insertIntoManagedObjectContext:context];
    
    return newAttach;
    
}



- (void)setupWithJson:(NSDictionary* )           json
            messageId:(NSString *)               parentMesaage
              context:(NSManagedObjectContext *) context {
    
    self.message = [[DataBaseManager sharedInstance] messagesGetWithMessageId:parentMesaage context:context];
    
    self.type = json[kType];
    
    
    
    SWITCH (self.type) {
        
        CASE (@"photo") {
            
            self.ident = [json[self.type][kChatId] stringValue];
            
            self.ownerId = [json[self.type][kOwnerId] stringValue];
            
            self.phototUrlSmall = json[self.type][@"photo_75"];
            
            self.photoUrlLarge = json[self.type][@"photo_604"];
            
            break;
            
        }
        
        CASE (@"video") {
            
            self.ident = [json[self.type][kChatId] stringValue];
            
            self.ownerId = [json[self.type][kOwnerId] stringValue];
            
            self.phototUrlSmall = json[self.type][@"photo_130"];
            
            self.photoUrlLarge = json[self.type][@"photo_320"];
            
            self.downloadUrl = json[self.type][@"player"];
            
            self.accessKey = [json[self.type][@"access_key"] stringValue];
            
            break;
            
        }
        
        CASE (@"audio") {
            
            self.ident = [json[self.type][kChatId] stringValue];
            
            self.ownerId = [json[self.type][kOwnerId] stringValue];
            
            self.downloadUrl = json[self.type][@"url"];
            
            self.title = [NSString stringWithFormat:@"%@\n%@",json[self.type][@"artist"],json[self.type][@"title"]];
            
            break;
            
        }
        
        CASE (@"doc") {
            
            self.ident = [json[self.type][kChatId] stringValue];
            
            self.ownerId = [json[self.type][kOwnerId] stringValue];
            
            self.title = json[self.type][@"title"];
            
            self.downloadUrl = json[self.type][@"url"];
            
            break;
            
        }
        
        CASE (@"link") {
            
            self.title = json[self.type][@"title"];
            
            self.downloadUrl = json[self.type][@"url"];
            
            self.phototUrlSmall = json[self.type][@"photo"][@"photo_75"];
            
            self.photoUrlLarge = json[self.type][@"photo"][@"photo_604"];
            
            break;
            
        }
        
        CASE (@"market") {
            
            self.ident = [json[self.type][kChatId] stringValue];
            
            self.ownerId = [json[self.type][kOwnerId] stringValue];
            
            self.title = json[self.type][@"title"];
            
            self.phototUrlSmall = json[self.type][@"thumb_photo"];
            
            self.photoUrlLarge = json[self.type][@"thumb_photo"];
            
            break;
            
        }
        
        CASE (@"market_album") {
            
            self.title = json[self.type][@"title"];
            
            self.ident = [json[self.type][kChatId] stringValue];
            
            self.ownerId = [json[self.type][kOwnerId] stringValue];;
            
            self.phototUrlSmall = json[self.type][@"photo"][@"photo_75"];
            
            self.photoUrlLarge = json[self.type][@"photo"][@"photo_604"];
            
            break;
            
        }
        
        CASE (@"wall") {
            
            self.ident = [json[self.type][kChatId] stringValue];
            
            self.ownerId = [json[self.type][kOwnerId] stringValue];
            
            self.title = json[self.type][@"text"];
            
            break;
            
        }
        
        CASE (@"wall_reply") {
            
            self.ownerId = [json[self.type][kOwnerId] stringValue];
            
            self.title = json[self.type][@"text"];
            
            break;
            
        }
        
        CASE (@"sticker") {
            
            self.ident = [json[self.type][kChatId] stringValue];
            
            self.phototUrlSmall = json[self.type][@"photo_64"];
            
            self.photoUrlLarge = json[self.type][@"photo_256"];
            
            break;
            
        }
        
        CASE (@"gift") {
            
            self.ident = [json[self.type][kChatId] stringValue];
            
            self.phototUrlSmall = json[self.type][@"thumb_256"];
            
            self.photoUrlLarge = json[self.type][@"thumb_48"];
            
            break;
            
        }
        
        DEFAULT {
            
            break;
            
        }
        
    }
    
    
    
}

@end

