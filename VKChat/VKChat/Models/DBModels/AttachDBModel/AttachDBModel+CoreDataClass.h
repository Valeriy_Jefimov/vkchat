//

//  AttachDBModel+CoreDataClass.h

//  VKChat

//

//  Created by Valery Efimov on 31.01.17.

//  Copyright © 2017 Valery Efimov. All rights reserved.

//



#import <Foundation/Foundation.h>

#import <CoreData/CoreData.h>



@class MessageDBModel;



NS_ASSUME_NONNULL_BEGIN



@interface AttachDBModel : NSManagedObject



- (void)setupWithJson:(NSDictionary* )           json
            messageId:(NSString *)               parentMesaage
              context:(NSManagedObjectContext *) context;

+ (instancetype)newEntityWithContext:(NSManagedObjectContext*)context;



@end



NS_ASSUME_NONNULL_END



#import "AttachDBModel+CoreDataProperties.h"

