//
//  AttachDBModel+CoreDataProperties.m
//  VKChat
//
//  Created by Efimov on 05.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "AttachDBModel+CoreDataProperties.h"

@implementation AttachDBModel (CoreDataProperties)

+ (NSFetchRequest<AttachDBModel *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AttachDBModel"];
}

@dynamic accessKey;
@dynamic downloadUrl;
@dynamic ident;
@dynamic ownerId;
@dynamic phototUrlSmall;
@dynamic photoUrlLarge;
@dynamic title;
@dynamic type;
@dynamic videoPlayerUrl;
@dynamic message;

@end
