//
//  MessageDBModel+CoreDataClass.h
//  VKChat
//
//  Created by Valery Efimov on 02.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AttachDBModel, ChatDBModel;

NS_ASSUME_NONNULL_BEGIN

@interface MessageDBModel : NSManagedObject

- (void)setupWithJson:               (NSDictionary*)                    json
              context:               (nonnull NSManagedObjectContext *) context;
+ (instancetype)newEntityWithContext:(NSManagedObjectContext*)          context;

@end

NS_ASSUME_NONNULL_END

#import "MessageDBModel+CoreDataProperties.h"
