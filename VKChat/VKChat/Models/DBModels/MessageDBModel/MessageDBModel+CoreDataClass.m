//
//  MessageDBModel+CoreDataClass.m
//  VKChat
//
//  Created by Valery Efimov on 01.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

//
//  MessageDBModel+CoreDataClass.m
//  VKChat
//
//  Created by Valery Efimov on 30.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//


#import "MessageDBModel+CoreDataClass.h"
#import "AttachDBModel+CoreDataClass.h"
#import "ChatDBModel+CoreDataClass.h"
#import "VKManager.h"
#import "VKManagerConstants.h"
#import "DataBaseManager.h"
#import "BaseTableViewCell.h"

static CGFloat  const kPart =                     0.7;

@implementation MessageDBModel

- (void)setupWithJson:(NSDictionary*)                    json
              context:(nonnull NSManagedObjectContext *) context {
    NSString *chatId          = [json[kMessageChatID] stringValue];
    if(!chatId)
    {
        chatId                = [json[kUserID] stringValue];
    }
    self.chat                 = [[DataBaseManager sharedInstance]messagesGetChatWithChatId:chatId
                                                                                   context:context];
    if(!self.chat)
    {
        NSString *fields      = @"photo_50, photo_100, photo_200_orig, online,  last_seen";
        self.chat             = [ChatDBModel newEntityWithContext:context];
        [VKManager messagesGetChatWithChatId:chatId
                                      fields:fields
                                  completion:^(NSDictionary *json) {
                                      [self.chat setupWithJson:json completion:^{}];
                                      self.chat.chatID  = chatId;
                                  }];
    }
    self.messageID            = [json[kMessageId] stringValue];
    self.body                 = json[kBody];
    self.adminId              = [json[kAdminId] stringValue];
    self.userID               = [json[kUserID] stringValue];
    self.title                = json[kTitle];
    self.date                 = [VKManager dateFormatter:json[kDate]];
    self.outState             = [json[kMessageOut] stringValue];
    self.usersCount           = [json[kUsersCount] stringValue];
    self.readState            = [json[kReadState] stringValue];
    self.unixTimeStamp        = [json[kDate]floatValue];
    self.photo50              = json[kPhoto50Url];
    self.photo100             = json[kPhoto100Url];
    self.photo200             = json[kPhoto200Url];
    NSString *text            = self.body;
    float dataViewWidth       = [UIScreen mainScreen].bounds.size.width * kPart;
    self.bodySizeWidth        = [BaseTableViewCell getSizeForText:text
                                                         maxWidth:dataViewWidth
                                                             font:@"Avenir-Roman"
                                                         fontSize:12].width;
    self.bodySizeHeight       = [BaseTableViewCell getSizeForText:text
                                                         maxWidth:dataViewWidth
                                                             font:@"Avenir-Roman"
                                                         fontSize:12].height;
    for (NSDictionary *attachJson in json[kAttachments])
    {
        AttachDBModel *attach = [AttachDBModel newEntityWithContext:context];
        [attach setupWithJson:attachJson
                    messageId:self.messageID
                      context:context];
        [self addAttachObject:attach];
    }
    for (NSDictionary *forwardMessageJson in json[kForwardMessages])
    {
        MessageDBModel *forwardMessage  = [MessageDBModel newEntityWithContext:context];
        [forwardMessage setupWithJson:forwardMessageJson
                              context:context];
        forwardMessage.parentMessage    = self;
        [self addForvardMessagesObject:forwardMessage];
    }
    self.creationDate = [NSDate new];
    // needs needs sender to be added
}


- (BOOL)compareTimeInMessageModel:(double)timeStamp {
    NSTimeInterval differ               = [[NSDate dateWithTimeIntervalSince1970:self.unixTimeStamp ] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:timeStamp]];
    if (differ > self.unixTimeStamp  + 1000)
    {
        return YES;
    }
    return NO;
}

+ (instancetype)newEntityWithContext:(NSManagedObjectContext*)context {
    NSEntityDescription *discr          = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                                      inManagedObjectContext:context];
    MessageDBModel * newMessage         = [[MessageDBModel alloc] initWithEntity:discr
                                                  insertIntoManagedObjectContext:context];
    
    return newMessage;
}

@end

