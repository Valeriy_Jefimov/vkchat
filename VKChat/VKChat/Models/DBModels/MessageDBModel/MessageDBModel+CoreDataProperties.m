//
//  MessageDBModel+CoreDataProperties.m
//  VKChat
//
//  Created by Efimov on 05.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "MessageDBModel+CoreDataProperties.h"

@implementation MessageDBModel (CoreDataProperties)

+ (NSFetchRequest<MessageDBModel *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MessageDBModel"];
}

@dynamic action;
@dynamic actionEmail;
@dynamic actionMId;
@dynamic actionText;
@dynamic adminId;
@dynamic body;
@dynamic bodySizeHeight;
@dynamic bodySizeWidth;
@dynamic creationDate;
@dynamic date;
@dynamic deletedState;
@dynamic emojiState;
@dynamic importantState;
@dynamic messageID;
@dynamic outState;
@dynamic photo50;
@dynamic photo100;
@dynamic photo200;
@dynamic randomID;
@dynamic readState;
@dynamic title;
@dynamic unixTimeStamp;
@dynamic userFromID;
@dynamic userID;
@dynamic usersCount;
@dynamic attach;
@dynamic chat;
@dynamic forvardMessages;
@dynamic lastMessageChatIn;
@dynamic parentMessage;
@dynamic sender;

@end
