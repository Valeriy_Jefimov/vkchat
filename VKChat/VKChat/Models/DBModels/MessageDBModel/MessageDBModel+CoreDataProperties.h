//
//  MessageDBModel+CoreDataProperties.h
//  VKChat
//
//  Created by Efimov on 05.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "MessageDBModel+CoreDataClass.h"
#import "UserDBModel+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MessageDBModel (CoreDataProperties)

+ (NSFetchRequest<MessageDBModel *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *action;
@property (nullable, nonatomic, copy) NSString *actionEmail;
@property (nullable, nonatomic, copy) NSString *actionMId;
@property (nullable, nonatomic, copy) NSString *actionText;
@property (nullable, nonatomic, copy) NSString *adminId;
@property (nullable, nonatomic, copy) NSString *body;
@property (nonatomic) float bodySizeHeight;
@property (nonatomic) float bodySizeWidth;
@property (nullable, nonatomic, copy) NSDate *creationDate;
@property (nullable, nonatomic, copy) NSString *date;
@property (nullable, nonatomic, copy) NSString *deletedState;
@property (nullable, nonatomic, copy) NSString *emojiState;
@property (nullable, nonatomic, copy) NSString *importantState;
@property (nullable, nonatomic, copy) NSString *messageID;
@property (nullable, nonatomic, copy) NSString *outState;
@property (nullable, nonatomic, copy) NSString *photo50;
@property (nullable, nonatomic, copy) NSString *photo100;
@property (nullable, nonatomic, copy) NSString *photo200;
@property (nullable, nonatomic, copy) NSString *randomID;
@property (nullable, nonatomic, copy) NSString *readState;
@property (nullable, nonatomic, copy) NSString *title;
@property (nonatomic) float unixTimeStamp;
@property (nullable, nonatomic, copy) NSString *userFromID;
@property (nullable, nonatomic, copy) NSString *userID;
@property (nullable, nonatomic, copy) NSString *usersCount;
@property (nullable, nonatomic, retain) NSSet<AttachDBModel *> *attach;
@property (nullable, nonatomic, retain) ChatDBModel *chat;
@property (nullable, nonatomic, retain) NSOrderedSet<MessageDBModel *> *forvardMessages;
@property (nullable, nonatomic, retain) ChatDBModel *lastMessageChatIn;
@property (nullable, nonatomic, retain) MessageDBModel *parentMessage;
@property (nullable, nonatomic, retain) UserDBModel *sender;

@end

@interface MessageDBModel (CoreDataGeneratedAccessors)

- (void)addAttachObject:(AttachDBModel *)value;
- (void)removeAttachObject:(AttachDBModel *)value;
- (void)addAttach:(NSSet<AttachDBModel *> *)values;
- (void)removeAttach:(NSSet<AttachDBModel *> *)values;

- (void)insertObject:(MessageDBModel *)value inForvardMessagesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromForvardMessagesAtIndex:(NSUInteger)idx;
- (void)insertForvardMessages:(NSArray<MessageDBModel *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeForvardMessagesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInForvardMessagesAtIndex:(NSUInteger)idx withObject:(MessageDBModel *)value;
- (void)replaceForvardMessagesAtIndexes:(NSIndexSet *)indexes withForvardMessages:(NSArray<MessageDBModel *> *)values;
- (void)addForvardMessagesObject:(MessageDBModel *)value;
- (void)removeForvardMessagesObject:(MessageDBModel *)value;
- (void)addForvardMessages:(NSOrderedSet<MessageDBModel *> *)values;
- (void)removeForvardMessages:(NSOrderedSet<MessageDBModel *> *)values;

@end

NS_ASSUME_NONNULL_END
