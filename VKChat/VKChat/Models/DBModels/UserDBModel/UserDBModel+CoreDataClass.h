//
//  UserDBModel+CoreDataClass.h
//  VKChat
//
//  Created by Valery Efimov on 01.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "UIImageView+Downloader.h"

@class ChatDBModel;

NS_ASSUME_NONNULL_BEGIN

@interface UserDBModel : NSManagedObject

- (UIImage *)          userImage;
- (void)               setupWithJson:(NSDictionary* )json
                             context:(NSManagedObjectContext *) context;
+ (instancetype)newEntityWithContext:(NSManagedObjectContext*)context;

@end

NS_ASSUME_NONNULL_END

#import "UserDBModel+CoreDataProperties.h"
