//
//  UserDBModel+CoreDataProperties.m
//  VKChat
//
//  Created by Efimov on 05.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "UserDBModel+CoreDataProperties.h"

@implementation UserDBModel (CoreDataProperties)

+ (NSFetchRequest<UserDBModel *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"UserDBModel"];
}

@dynamic firstName;
@dynamic lastName;
@dynamic lastSeenPlatform;
@dynamic lastSeenTime;
@dynamic online;
@dynamic photo50Url;
@dynamic photo100Url;
@dynamic photo200Url;
@dynamic userID;
@dynamic chat;
@dynamic chatInWichSelfIsCurrent;
@dynamic adminedChat;
@dynamic messages;

@end
