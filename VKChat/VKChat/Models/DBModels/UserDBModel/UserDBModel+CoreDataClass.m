//
//  UserDBModel+CoreDataClass.m
//  VKChat
//
//  Created by Valery Efimov on 01.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "UserDBModel+CoreDataClass.h"
#import "ChatDBModel+CoreDataClass.h"
#import "VKManager.h"
#import "VKManagerConstants.h"
#import "DataBaseManager.h"

@implementation UserDBModel


- (void)setupWithJson:(NSDictionary* )json context:(NSManagedObjectContext *) context {
    self.firstName                  = json[kFirstName] ;
    self.userID                     = [json[kUserId]stringValue];
    self.lastName                   = json[kLastName];
    self.photo50Url                 = json[kPhoto50Url];
    self.photo100Url                = json[kPhoto100Url];
    self.photo200Url                = json[kPhoto200Url];
    self.online                     = [json[kOnline]stringValue];
    self.lastSeenTime               = [json[kLastSeen][kTime]stringValue];
    self.lastSeenPlatform           = [json[kLastSeen][kPlatform]stringValue];
    ChatDBModel *chat               = [[DataBaseManager sharedInstance] messagesGetChatWithChatId: self.userID
                                                                                          context:context];
    if(chat)
    {
        [self addChatObject:chat];
    }
}

+ (instancetype)newEntityWithContext:(NSManagedObjectContext*)context {
    NSEntityDescription *discr      = [NSEntityDescription entityForName:NSStringFromClass([self class]) inManagedObjectContext:context];
    UserDBModel * newUser           = [[UserDBModel alloc] initWithEntity:discr
                                           insertIntoManagedObjectContext:context];
    return newUser;
}

- (UIImage *)userImage {
    NSString *url                   = [NSString new];
    url                             = self.photo100Url;
    NSString *name                  = [self prepareImagePathToSave:url];
    __block UIImage *imageFromDB    = [DataBaseManager imagefromDocumentsDirectoryWithName:[NSString stringWithFormat:@"%@",name]];
    if(!imageFromDB)
    {
        [UIImageView loadImageWithURL:[NSURL URLWithString:url]
                       WithCompletion:^(UIImage *image) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [DataBaseManager saveToDocumentsImageWithImage:image imageName:name];
                               imageFromDB = image;
                           });
                       }];
    }
    return imageFromDB;
}

- (NSString *)prepareImagePathToSave:(NSString *) path {
    path                            = [path stringByReplacingOccurrencesOfString:@"/" withString:@""];
    path                            = [path stringByReplacingOccurrencesOfString:@"//" withString:@""];
    path                            = [path stringByReplacingOccurrencesOfString:@"http" withString:@""];
    path                            = [path stringByReplacingOccurrencesOfString:@"jpg." withString:@""];
    path                            = [path stringByReplacingOccurrencesOfString:@"s:pp.vk.mec." withString:@""];
    
    return path;
}


@end

