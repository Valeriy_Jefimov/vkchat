//
//  UserDBModel+CoreDataProperties.h
//  VKChat
//
//  Created by Efimov on 05.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "UserDBModel+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface UserDBModel (CoreDataProperties)

+ (NSFetchRequest<UserDBModel *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *firstName;
@property (nullable, nonatomic, copy) NSString *lastName;
@property (nullable, nonatomic, copy) NSString *lastSeenPlatform;
@property (nullable, nonatomic, copy) NSString *lastSeenTime;
@property (nullable, nonatomic, copy) NSString *online;
@property (nullable, nonatomic, copy) NSString *photo50Url;
@property (nullable, nonatomic, copy) NSString *photo100Url;
@property (nullable, nonatomic, copy) NSString *photo200Url;
@property (nullable, nonatomic, copy) NSString *userID;
@property (nullable, nonatomic, retain) NSOrderedSet<ChatDBModel *> *chat;
@property (nullable, nonatomic, retain) ChatDBModel *chatInWichSelfIsCurrent;
@property (nullable, nonatomic, retain) ChatDBModel *adminedChat;
@property (nullable, nonatomic, retain) NSSet<UserDBModel *> *messages;

@end

@interface UserDBModel (CoreDataGeneratedAccessors)

- (void)insertObject:(ChatDBModel *)value inChatAtIndex:(NSUInteger)idx;
- (void)removeObjectFromChatAtIndex:(NSUInteger)idx;
- (void)insertChat:(NSArray<ChatDBModel *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeChatAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInChatAtIndex:(NSUInteger)idx withObject:(ChatDBModel *)value;
- (void)replaceChatAtIndexes:(NSIndexSet *)indexes withChat:(NSArray<ChatDBModel *> *)values;
- (void)addChatObject:(ChatDBModel *)value;
- (void)removeChatObject:(ChatDBModel *)value;
- (void)addChat:(NSOrderedSet<ChatDBModel *> *)values;
- (void)removeChat:(NSOrderedSet<ChatDBModel *> *)values;

- (void)addMessagesObject:(UserDBModel *)value;
- (void)removeMessagesObject:(UserDBModel *)value;
- (void)addMessages:(NSSet<UserDBModel *> *)values;
- (void)removeMessages:(NSSet<UserDBModel *> *)values;

@end

NS_ASSUME_NONNULL_END
