//
//  CurrentUserDBModel+CoreDataProperties.h
//  VKChat
//
//  Created by Valery Efimov on 31.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "CurrentUserDBModel+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CurrentUserDBModel (CoreDataProperties)

+ (NSFetchRequest<CurrentUserDBModel *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSOrderedSet<ChatDBModel *> *currentUsersChats;

@end

@interface CurrentUserDBModel (CoreDataGeneratedAccessors)

- (void)insertObject:(ChatDBModel *)value inCurrentUsersChatsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromCurrentUsersChatsAtIndex:(NSUInteger)idx;
- (void)insertCurrentUsersChats:(NSArray<ChatDBModel *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeCurrentUsersChatsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInCurrentUsersChatsAtIndex:(NSUInteger)idx withObject:(ChatDBModel *)value;
- (void)replaceCurrentUsersChatsAtIndexes:(NSIndexSet *)indexes withCurrentUsersChats:(NSArray<ChatDBModel *> *)values;
- (void)addCurrentUsersChatsObject:(ChatDBModel *)value;
- (void)removeCurrentUsersChatsObject:(ChatDBModel *)value;
- (void)addCurrentUsersChats:(NSOrderedSet<ChatDBModel *> *)values;
- (void)removeCurrentUsersChats:(NSOrderedSet<ChatDBModel *> *)values;

@end

NS_ASSUME_NONNULL_END
