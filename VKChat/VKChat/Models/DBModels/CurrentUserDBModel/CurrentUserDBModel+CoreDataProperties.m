//
//  CurrentUserDBModel+CoreDataProperties.m
//  VKChat
//
//  Created by Valery Efimov on 31.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "CurrentUserDBModel+CoreDataProperties.h"

@implementation CurrentUserDBModel (CoreDataProperties)

+ (NSFetchRequest<CurrentUserDBModel *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"CurrentUserDBModel"];
}

@dynamic currentUsersChats;

@end
