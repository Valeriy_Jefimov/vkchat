//
//  CurrentUserDBModel+CoreDataClass.h
//  VKChat
//
//  Created by Valery Efimov on 31.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDBModel+CoreDataClass.h"

@class ChatDBModel;

NS_ASSUME_NONNULL_BEGIN

@interface CurrentUserDBModel : UserDBModel

@end

NS_ASSUME_NONNULL_END

#import "CurrentUserDBModel+CoreDataProperties.h"
