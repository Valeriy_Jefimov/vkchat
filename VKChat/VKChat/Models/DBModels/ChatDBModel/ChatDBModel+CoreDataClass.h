//
//  ChatDBModel+CoreDataClass.h
//  VKChat
//
//  Created by Valery Efimov on 02.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

@class CurrentUserDBModel, MessageDBModel, UserDBModel;

NS_ASSUME_NONNULL_BEGIN

@interface ChatDBModel : NSManagedObject

- (void)setupWithJson:               (NSDictionary *)                    json
           completion:               (nonnull void (^)())                completionBlock;
- (void)getUsersWithUserIDs:         (NSArray *)                         IDs
                    context:         (NSManagedObjectContext *)          context
                 completion:         (void (^)())                        completionBlock;
+ (instancetype)newEntityWithContext:(NSManagedObjectContext *)          context;
- (void)setupChatWithNotification:   (NSNotification *)                  notification
                          context:   (NSManagedObjectContext *)          context;
- (void)setupDialogWithNotification: (NSNotification *)                  notification
                            context: (NSManagedObjectContext *)          context
                             chatId: (NSString*)                         chatId;

- (UIImage *)chatImage;
- (NSString *)currentUserPlatformImage;

@end

NS_ASSUME_NONNULL_END

#import "ChatDBModel+CoreDataProperties.h"

