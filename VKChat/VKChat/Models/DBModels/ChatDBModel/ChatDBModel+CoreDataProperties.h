//
//  ChatDBModel+CoreDataProperties.h
//  VKChat
//
//  Created by Efimov on 05.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "ChatDBModel+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ChatDBModel (CoreDataProperties)

+ (NSFetchRequest<ChatDBModel *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *body;
@property (nullable, nonatomic, copy) NSString *chatID;
@property (nullable, nonatomic, copy) NSDate *creationDate;
@property (nullable, nonatomic, copy) NSString *image100;
@property (nullable, nonatomic, copy) NSString *inReadID;
@property (nonatomic) BOOL isDownload;
@property (nonatomic) BOOL isInRead;
@property (nonatomic) BOOL isNeedUpdatePhoto;
@property (nonatomic) BOOL isOutRead;
@property (nonatomic) BOOL isP2P;
@property (nonatomic) BOOL isPhoto;
@property (nullable, nonatomic, copy) NSString *messagesMaxCount;
@property (nullable, nonatomic, copy) NSString *outBox;
@property (nullable, nonatomic, copy) NSString *outReadID;
@property (nullable, nonatomic, copy) NSString *readState;
@property (nullable, nonatomic, copy) NSString *time;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *unread;
@property (nullable, nonatomic, copy) NSString *unreadedCount;
@property (nullable, nonatomic, copy) NSString *usersCount;
@property (nullable, nonatomic, retain) CurrentUserDBModel *currentUser;
@property (nullable, nonatomic, retain) MessageDBModel *lastMessageModel;
@property (nullable, nonatomic, retain) UserDBModel *lastUserModel;
@property (nullable, nonatomic, retain) NSOrderedSet<MessageDBModel *> *messages;
@property (nullable, nonatomic, retain) NSOrderedSet<UserDBModel *> *users;
@property (nullable, nonatomic, retain) UserDBModel *admin;

@end

@interface ChatDBModel (CoreDataGeneratedAccessors)

- (void)insertObject:(MessageDBModel *)value inMessagesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromMessagesAtIndex:(NSUInteger)idx;
- (void)insertMessages:(NSArray<MessageDBModel *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeMessagesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInMessagesAtIndex:(NSUInteger)idx withObject:(MessageDBModel *)value;
- (void)replaceMessagesAtIndexes:(NSIndexSet *)indexes withMessages:(NSArray<MessageDBModel *> *)values;
- (void)addMessagesObject:(MessageDBModel *)value;
- (void)removeMessagesObject:(MessageDBModel *)value;
- (void)addMessages:(NSOrderedSet<MessageDBModel *> *)values;
- (void)removeMessages:(NSOrderedSet<MessageDBModel *> *)values;

- (void)insertObject:(UserDBModel *)value inUsersAtIndex:(NSUInteger)idx;
- (void)removeObjectFromUsersAtIndex:(NSUInteger)idx;
- (void)insertUsers:(NSArray<UserDBModel *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeUsersAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInUsersAtIndex:(NSUInteger)idx withObject:(UserDBModel *)value;
- (void)replaceUsersAtIndexes:(NSIndexSet *)indexes withUsers:(NSArray<UserDBModel *> *)values;
- (void)addUsersObject:(UserDBModel *)value;
- (void)removeUsersObject:(UserDBModel *)value;
- (void)addUsers:(NSOrderedSet<UserDBModel *> *)values;
- (void)removeUsers:(NSOrderedSet<UserDBModel *> *)values;

@end

NS_ASSUME_NONNULL_END
