//
//  ChatDBModel+CoreDataProperties.m
//  VKChat
//
//  Created by Efimov on 05.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "ChatDBModel+CoreDataProperties.h"

@implementation ChatDBModel (CoreDataProperties)

+ (NSFetchRequest<ChatDBModel *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ChatDBModel"];
}

@dynamic body;
@dynamic chatID;
@dynamic creationDate;
@dynamic image100;
@dynamic inReadID;
@dynamic isDownload;
@dynamic isInRead;
@dynamic isNeedUpdatePhoto;
@dynamic isOutRead;
@dynamic isP2P;
@dynamic isPhoto;
@dynamic messagesMaxCount;
@dynamic outBox;
@dynamic outReadID;
@dynamic readState;
@dynamic time;
@dynamic title;
@dynamic unread;
@dynamic unreadedCount;
@dynamic usersCount;
@dynamic currentUser;
@dynamic lastMessageModel;
@dynamic lastUserModel;
@dynamic messages;
@dynamic users;
@dynamic admin;

@end
