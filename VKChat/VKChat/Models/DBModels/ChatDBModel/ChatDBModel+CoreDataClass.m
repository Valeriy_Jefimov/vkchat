//
//  ChatDBModel+CoreDataClass.m
//  VKChat
//
//  Created by Valery Efimov on 02.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//
//
//  ChatDBModel+CoreDataClass.m
//  VKChat
//
//  Created by Valery Efimov on 01.02.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.


#import "ChatDBModel+CoreDataClass.h"
#import "MessageDBModel+CoreDataClass.h"
#import "UserDBModel+CoreDataClass.h"
#import "VKManagerConstants.h"
#import "VKManager.h"
#import "DataBaseManager.h"
#import "UIImageView+Downloader.h"
#import "CurrentUserDBModel+CoreDataClass.h"

@implementation ChatDBModel

- (void)setupWithJson:(NSDictionary *)json
           completion:(nonnull void (^)())completionBlock  {
    self.isDownload                 = NO;
    self.isInRead                   = NO;
    self.isOutRead                  = NO;
    NSDictionary *message           = json[kMessage];
    NSManagedObjectContext *context = self.managedObjectContext;
    
    
    //needs to be dowloaded from db
    
    
    MessageDBModel *last            = [MessageDBModel newEntityWithContext:context];
    [last setupWithJson:message
                context:context];
    self.lastMessageModel           = last;
    
    if(self.chatID && self.image100)
    {
        self.isPhoto                = YES;
    }
    else
    {
        self.isPhoto                = NO;
    }
    self.unreadedCount              = [json[@"unread"]stringValue];;
    self.creationDate               = [NSDate date];
    if(!self.unreadedCount)
    {
        self.unreadedCount          = @"0";
    }
    if (![[NSString stringWithFormat:@"%@",self.unread] isEqualToString:@"0"])
    {
        self.unreadedCount          = @"0";
    }
    self.inReadID                   = [json[@"in_read"]stringValue];
    self.outReadID                  = [json[@"out_read"]stringValue];
    self.unread                     = self.lastMessageModel.outState;
    self.lastMessageID              = self.lastMessageModel.messageID;
    self.adminID                    = self.lastMessageModel.adminId;
    self.chatID                     = self.lastMessageModel.chatID;
    if(!self.chatID)
    {
        self.chatID                 = self.userID;
        self.isP2P                  = NO;
    }
    else
    {
        self.isP2P                  = YES;
    }
    self.image100                   = self.lastMessageModel.photo100;
    self.time                       = self.lastMessageModel.date;
    self.body                       = self.lastMessageModel.body;
    self.usersCount                 = self.lastMessageModel.usersCount;
    self.readState                  = self.lastMessageModel.readState;
    self.userID                     = self.lastMessageModel.userID;
    NSString *type                  = message[@"attachments"];
    self.title                      = self.lastMessageModel.title;
    
    if (message[@"fwd_messages"])
    {
        self.body                   = NSLocalizedString(@"Forward message",nil);
    }
    if (type)
    {
        self.body                   = NSLocalizedString(@"Attachment",nil);
    }
    completionBlock();
}

- (void)getUsersWithUserIDs:(NSArray *)               IDs
                    context:(NSManagedObjectContext *)context
                 completion: (void (^)())completionBlock {
    [VKManager usersGetWithUserID:IDs
                       completion:^(NSArray *users) {
                           for(int i = 0; i < users.count; i ++)
                           {
                               NSDictionary *userDict      = [NSDictionary dictionaryWithDictionary:users[i]];
                               UserDBModel *user           = [[DataBaseManager sharedInstance] usersGetWithUserId:userDict[kChatId] context:context];
                               if(!user)
                               {
                                   user                    = [UserDBModel newEntityWithContext: context];
                                   [user setupWithJson:userDict
                                               context:context];
                                   [self addUsersObject:user];
                               }
                               else
                               {
                                   [self addUsersObject:user];
                               }
                           }
                           UserDBModel *lastUser           = [self.users array][users.count - 1];
                           self.userOnline                 = [NSString stringWithFormat:@"%@",lastUser.online];
                           self.lastMessageUserPlatform    = [NSString stringWithFormat:@"%@",lastUser.lastSeenPlatform];
                           if ([self.title isEqualToString:@" ... "])
                           {
                               
                               self.title                  = [NSString stringWithFormat:@"%@ %@",lastUser.firstName, lastUser.lastName];
                           }
                           
                           completionBlock();
                       }];
}

+ (instancetype)newEntityWithContext:(NSManagedObjectContext *)context {
    NSEntityDescription *discr          = [NSEntityDescription entityForName:NSStringFromClass([self class]) inManagedObjectContext:context];
    ChatDBModel * newChat               = [[ChatDBModel alloc] initWithEntity:discr insertIntoManagedObjectContext:context];
    return newChat;
}

- (NSString *)currentUserPlatformImage {
    if(self.isP2P)
    {
        return  @"group2@x";
    }
    else
    {
        if([self.userOnline isEqualToString:@"1"])
        {
            if([self.lastMessageUserPlatform integerValue] > 4 || [self.lastMessageUserPlatform integerValue] < 2)
            {
                return  @"web2@x";
            }
            else
            {
                return @"phone2@x";
            }
        }
    }
    return nil;
}

#pragma mark - Notification Setup


- (void)setupDialogWithNotification:(NSNotification *)          notification
                            context:(NSManagedObjectContext *)  context
                             chatId: (NSString*)                chatId {
    NSArray * ids                     = [NSArray arrayWithObject:[NSString stringWithFormat:@"%@",notification.object[1]]];
    
    [VKManager messagesGetWithIds:ids
                    previewLength:@"0"
                       completion:^(NSDictionary *item) {
                           ChatDBModel* chat             = [[DataBaseManager sharedInstance] messagesGetChatWithChatId:chatId context:context];
                           MessageDBModel *message       = [MessageDBModel newEntityWithContext:context];
                           [message setupWithJson:item
                                          context:context];
                           [chat addMessagesObject:message];
                           if (message.unixTimeStamp  > chat.lastMessageModel.unixTimeStamp)
                           {
                               chat.lastMessageModel      = message;
                           }
                           chat.creationDate              = [NSDate date];
                           [[DataBaseManager sharedInstance]saveContext:context];
                       }];
}

- (void)setupChatWithNotification:(NSNotification *)         notification
                          context:(NSManagedObjectContext *) context {
    NSInteger str                      = [[NSString stringWithFormat:@"%@",self.unreadedCount] integerValue] + 1;
    
    self.unreadedCount                 = [NSString stringWithFormat:@"%ld",(long)str];
    self.time                          = [VKManager dateFormatter:[notification object][4]];
    self.lastMessageID                 = [NSString stringWithFormat:@"%@",notification.object[1]];
    self.lastUser                      = [NSString stringWithFormat:@"%ld",[notification.object[7][@"from"]integerValue]];
    NSArray *flags                     = [LongPollManager decodeFlags:[[NSString stringWithFormat:@"%@",[notification object][2]]integerValue]];
    if(flags.count > 1)
    {
        if([[NSString stringWithFormat:@"%@",flags[1]]isEqualToString:@"2"]  )
        {
            self.unread                = @"1";
        }
        else
        {
            self.unread                = @"0";
        }
    }
    else
    {
        if([[NSString stringWithFormat:@"%@",flags[0]]isEqualToString:@"1"])
        {
            self.unread                = @"0";
        }
    }
    self.readState                     = @"0";
    self.body                          = [NSString stringWithFormat:@"%@",[notification object][6]];
    self.creationDate                  = [NSDate date];
}


#pragma mark - Images


- (UIImage *)chatImage {
    NSPredicate *predicate             = [NSPredicate predicateWithFormat: @"userID == %@",self.userID];
    NSManagedObjectContext *context    = [[DataBaseManager sharedInstance] childManagedObjectContext];
    UserDBModel *user                  = [[[self.users array] filteredArrayUsingPredicate:predicate]firstObject];
    NSString *url                      = [NSString new];
    url                                = user.photo100Url;
    if(!url)
    {
        UserDBModel *model             = [DataBaseManager usersGetCurrentUserInContext:context ];
        url                            = [NSString new];
        url                            = model.photo100Url;
    }
    NSString *name                     = [self prepareImagePathToSave:url];
    __block UIImage *imageFromDB       = [DataBaseManager imagefromDocumentsDirectoryWithName:[NSString stringWithFormat:@"%@",name]];
    if(!imageFromDB)
    {
        [UIImageView loadImageWithURL:[NSURL URLWithString:url]
                       WithCompletion:^(UIImage *image) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [DataBaseManager saveToDocumentsImageWithImage:image imageName:name];
                               imageFromDB = image;
                           });
                       }];
    }
    return imageFromDB;
}

- (NSString *)prepareImagePathToSave:(NSString *) path {
    path                                = [path stringByReplacingOccurrencesOfString:@"/" withString:@""];
    path                                = [path stringByReplacingOccurrencesOfString:@"//" withString:@""];
    path                                = [path stringByReplacingOccurrencesOfString:@"http" withString:@""];
    path                                = [path stringByReplacingOccurrencesOfString:@"jpg." withString:@""];
    path                                = [path stringByReplacingOccurrencesOfString:@"s:pp.vk.mec." withString:@""];
    
    return path;
}


@end

