//
//  VKManager.h
//  VKChat
//
//  Created by Valery Efimov on 10.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <VKSdk.h>
#import "LongPollManager.h"
#import <VKApi.h>
#import "VKManagerConstants.h"


@interface VKManager : NSObject<VKSdkDelegate,NSURLSessionDelegate>

+ (void)imageWithURL:(NSURL *)url
             success:(void (^)(UIImage *image))success
             failure:(void (^)(NSError *error))failure;

//Audio
- (void)audioGetWithCount:(NSString *)count
                   offset:(NSString *)offsset
               completion: (void (^)(NSArray * items,NSInteger count))completionBlock;

- (void)audioGetById:(NSString *)audioId
          completion: (void (^)(VKAudio * audio))completionBlock;

//Docs
- (void)docsAddWithID:(NSArray *)docIDs
            accessKey:(NSString *)accessKey
           completion: (void (^)(VKDocs *doc))completionBlock;

- (void)docsGetWithCount:(NSString *)count
                  offset:(NSString *)offset
                    type:(NSString *)type
              completion: (void (^)(NSArray * items,NSInteger count))completionBlock;

- (void)docsUpload:(NSString *)doc withTitle:(NSString *)title
        completion: (void (^)(VKDocs * doc))completionBlock;
- (void)deleteWithID:(NSString *)docID;

- (void)login;
- (void)autorize;

//Account
+ (void)registreDeviceWithDeviceToken:(NSData*)deviceToken;
- (void)setOffline;
- (void)setOnline;
- (void)setPushSettings;
- (void)setSilenceMode;
- (void)unregisterDevice;

//Users
+ (void)usersGetWithUserID:(NSArray*)userID
                completion: (void (^)(NSArray *users))completionBlock;


- (void)chekReachable;

+ (NSString *)dateFormatter:(NSString *) date;

//Friends
- (void)friendsSearchWithQuery:(NSString *)query
                         count:(NSString *)count
                        offset:(NSString *)offset
                    completion: (void (^)(NSArray *items))completionBlock;

- (void)friendsGetOnlineWithCompletion: (void (^)(NSArray *items))completionBlock;


//Messages
- (void)getDialogsWithOffset:(NSString *)offset
                       count:(NSString *)count
              startMessageID:(NSString *)startMessageID
                 needsUnread:(NSString *)needsUnread
              needsImportant:(NSString *)needsImportant
             needsUnAnswered:(NSString *)needsUnAnswered
                  completion:(void (^)(NSArray * items,NSInteger count,NSInteger unreadCount)) completionBlock;

- (void)messagesDeleteDialogWithUser:(NSString *) userId
                              peerId:(NSString*) peerId
                          completion: (void (^)())completionBlock;

- (void)messagesSearchDialogsWithQ:(NSString *) q
                             limit:(NSString *) limit
                        completion: (void (^)(NSArray * items))completionBlock;

+ (void)getHistoryWithOffset:(NSString *)offset
                       count:(NSString *)count
                      userId:(NSString *)userId
                      peerId:(NSString *)peerId
              startMessageId:(NSString *)startMessageId
                         rev:(NSString *)rev
                  completion:(void (^)(NSDictionary * item)) completionBlock;
+ (void)messagesGetChatWithChatId:(NSString *)chatId
                           fields:(NSString *)fields
                       completion:(void (^)(NSDictionary * json))completionBlock;
+ (void)messagesGetDialogWithUserId:(NSString *)userId
                             fields:(NSString *)fields
                         completion:(void (^)(NSDictionary * json))completionBlock;
+ (void)messagesGetWithIds:(NSArray *) ids
             previewLength:(NSString *) length
                completion:(void (^)(NSDictionary * item)) completionBlock;

@property(assign,nonatomic) BOOL isConnect;


- (void)setUIDelegate:(id<VKSdkUIDelegate>)aDelegate;


@property(strong,nonatomic) NSMutableArray *userChats;
@property(strong,nonatomic) NSMutableArray *userFriends;
@property(strong,nonatomic) NSString *ownerOfAlbumID;
@property(strong,nonatomic) NSString *accessToken;
@property(strong,nonatomic) NSString *userID;

@end
