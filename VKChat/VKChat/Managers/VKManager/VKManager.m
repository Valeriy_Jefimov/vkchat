;//
//  VKManager.m
//  VKChat
//
//  Created by Valery Efimov on 10.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "VKManager.h"
#import "Reachability.h"
#import "DataBaseManager.h"


@interface VKManager ()
@property(strong,nonatomic) LongPollManager *longPollManager;
@property(strong,nonatomic) VKSdk *sdkInstance;
@property(strong,nonatomic) NSMutableArray *scope;
@property(strong,nonatomic) NSData *deviceToken;
@property(strong,nonatomic) NSURLSession *session;
@property(strong,nonatomic) Reachability* reach;



@end


@implementation VKManager


- (instancetype)init {
    self = [super init];
    
    if (self)
    {
        [self setupSession];
        self.userChats = [NSMutableArray new];
        self.userFriends = [NSMutableArray new];
        self.sdkInstance = [VKSdk initializeWithAppId:kAppId];
        if ([VKSdk initialized])
        {
            [self.sdkInstance registerDelegate:self];
            self.scope = @[kImageScope,kMessagesScope,kAudioScope,kVideoScope,kOfflineScope,kFriendsScope].mutableCopy;
        }
    }
    return self;
}

- (void)setupSession {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration
                                                          delegate:self
                                                     delegateQueue:nil];
    self.session = session;
}


#pragma mark - VKSdk


- (void)setUIDelegate:(id<VKSdkUIDelegate>)aDelegate {
    if([VKSdk initialized])
    {
        [self.sdkInstance setUiDelegate:aDelegate];
    }
}


#pragma mark - Login


- (void)login {
    [VKSdk wakeUpSession:self.scope completeBlock:^(VKAuthorizationState state, NSError *error) {
        switch (state)
        {
            case VKAuthorizationAuthorized:
            {
                [self setupAppSettingsWithCompletion:^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"authUnNeed" object:nil];
                }];
            }
                break;
                
            case VKAuthorizationInitialized:
                
                break;
                
            default:
                // Ask user  to try later
                break;
        }
    }];
    
    
}

- (void)autorize {
    [VKSdk authorize:self.scope];
}

- (void)setupAppSettingsWithCompletion: (void (^)())completionBlock {
    [self createCurrentUserObjectWithUserId:[NSNumber numberWithInt:[[[VKSdk accessToken] userId] intValue]]completion:^(NSDictionary *json) {
        [DataBaseManager usersCreateNewCurrentUserWithDictionary:json];
        completionBlock();
        self.longPollManager = [[LongPollManager alloc]initWithAccessToken:[[VKSdk accessToken] accessToken]];
        // [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert)];
        
        //    [[NSNotificationCenter defaultCenter] addObserver:self
        //                                             selector:@selector(networkFailed:)
        //                                                 name:@"networkFailed"
        //                                               object:nil];
        //
        //    [[NSNotificationCenter defaultCenter] addObserver:self
        //                                             selector:@selector(networkRestored:)
        //                                                 name:@"networkRestored"
        //                                               object:nil];
        
        
    }];
}


# pragma mark - Account


+ (void)registreDeviceWithDeviceToken:(NSData*)deviceToken {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        VKRequest *registerDeviceRequest = [VKApi requestWithMethod:kRegistreDeviceMethod andParameters:[NSDictionary dictionaryWithObjectsAndKeys:deviceToken, kResponseTypeEndPoint, @"msg", nil]];
        [registerDeviceRequest executeWithResultBlock:^(VKResponse *response) {
            NSLog(@"Successfully registered %@", deviceToken);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
          //  [error.vkError.request repeat];
        }];
    });
    
}

- (void)setOffline {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        VKRequest *setOfflineRequest = [VKApi requestWithMethod:kSetOfflineMethod andParameters:nil];
        [setOfflineRequest executeWithResultBlock:^(VKResponse *response) {
            NSLog(@"Response set offline %@", response.json);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
          //  [error.vkError.request repeat];
        }];
    });
}

- (void)setOnline {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        VKRequest *setOnlineRequest = [VKApi requestWithMethod:kSetOnlineMethod andParameters:[NSDictionary dictionaryWithObjectsAndKeys:@"0",@"voip", nil]];
        [setOnlineRequest executeWithResultBlock:^(VKResponse *response) {
            NSLog(@"Response setOnline %@", response.json);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
           // [error.vkError.request repeat];
        }];
    });
}

- (void)setPushSettings {
    
}

- (void)setSilenceMode {
    
}

- (void)unregisterDevice {
    
}


#pragma mark - Audio

- (void)audioGetWithCount:(NSString *) count
                   offset:(NSString *) offsset
               completion: (void (^)(NSArray * items,NSInteger count))completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        VKRequest *getAudioRequest = [VKApi requestWithMethod:kAudioGetMethod andParameters:[NSDictionary dictionaryWithObjectsAndKeys:self.userID,kOwnerId,offsset,kOffsetParametr,count,kCountChatsParametr, nil]];
        [getAudioRequest executeWithResultBlock:^(VKResponse *response) {
            completionBlock (response.json[kItems],[response.json[kCountChats] integerValue]);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
           // [error.vkError.request repeat];
        }];
    });
    
}

- (void)audioGetById:(NSString *) audioId
          completion: (void (^)(VKAudio * audio))completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        VKRequest *getAudioByIdRequest = [VKApi requestWithMethod:kAudioGetByIdMethod andParameters:[NSDictionary dictionaryWithObjectsAndKeys:@"audios",[NSString stringWithFormat:@"%@_%@",self.userID,audioId], nil]];
        [getAudioByIdRequest executeWithResultBlock:^(VKResponse *response) {
            VKAudio *audio = [[VKAudio alloc]initWithDictionary:response.json];
            completionBlock (audio);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
         //   [error.vkError.request repeat];
        }];
    });
}


#pragma mark - Docs


- (void)docsAddWithID:(NSArray *)docIDs
            accessKey:(NSString *)accessKey
           completion: (void (^)(VKDocs *doc))completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        VKRequest * request = [[VKApiDocs new] getByID:docIDs];
        [request executeWithResultBlock:^(VKResponse *response) {
            completionBlock([[VKDocs alloc]initWithDictionary:response.json]);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
          //  [error.vkError.request repeat];
        }];
    });
}

- (void)docsGetWithCount:(NSString *) count
                  offset:(NSString *) offset
                    type:(NSString *) type
              completion: (void (^)(NSArray * items,NSInteger count))completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        VKRequest * request = [[VKApiDocs new] get:[count integerValue] andOffset:[offset integerValue]];
        [request executeWithResultBlock:^(VKResponse *response) {
            completionBlock (response.json[kItems],[response.json[kCountChats] integerValue]);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
          //  [error.vkError.request repeat];
        }];
    });
}


- (void)docsUpload:(NSString *) doc withTitle:(NSString *) title completion: (void (^)(VKDocs * doc))completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        VKRequest * requestToUploadServer = [[VKApiDocs new] getUploadServer];
        [requestToUploadServer executeWithResultBlock:^(VKResponse *response) {
            VKRequest * request = [[VKApiDocs new]
                                   save:response.json[kUploadUrl]
                                   andTitle:title];
            [request executeWithResultBlock:^(VKResponse *response) {
                completionBlock([[VKDocs alloc]initWithDictionary:response.json]);
            } errorBlock:^(NSError *error) {
                NSLog(@"%@", error);
            }];
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
          //  [error.vkError.request repeat];
        }];
    });
}

- (void)deleteWithID:(NSString *) docID {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        VKRequest * request = [[VKApiDocs new] delete:[self.userID integerValue] andDocID:[docID integerValue]];
        [request executeWithResultBlock:^(VKResponse *response) {
            NSLog(@"success");
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
         //   [error.vkError.request repeat];
        }];
    });
}


#pragma mark - Friends


- (void)friendsGetWithCompletion: (void (^)(NSMutableArray *items))completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        __block   NSMutableArray *friendsList;
        
        VKRequest * request = [[VKApiFriends new] get:[NSDictionary dictionaryWithObjectsAndKeys:self.userID,kUsersID,@"hints",kOrder,@"5",kCountChats,kFriendsFields,kFriendsFieldsParametr,kNameCase,kNameCaseParametr,nil]];
        
        [request executeWithResultBlock:^(VKResponse *response) {
            friendsList = [NSArray arrayWithArray:response.json[kItems]].mutableCopy;
            if(friendsList.count > 1)
            {
                VKRequest * requestNameOrder = [[VKApiFriends new] get:[NSDictionary dictionaryWithObjectsAndKeys:self.userID,kUsersID,@"name",kOrder,@"0",kCountChats,kFriendsFields,kFriendsFieldsParametr,kNameCase,kNameCaseParametr,nil]];
                
                [requestNameOrder executeWithResultBlock:^(VKResponse *response) {
                    NSMutableArray *friendsListInNameOrder = [NSMutableArray arrayWithArray:response.json[kItems]];
                    [friendsList addObjectsFromArray:friendsListInNameOrder];
                    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:friendsList];
                    friendsList = [orderedSet array].mutableCopy;
                 //   self.friends = friendsList;
                    completionBlock(friendsList);
                } errorBlock:^(NSError *error) {
                    NSLog(@"%@", error);
                }];
            } else
            {
                //self.friends = friendsList;
               // completionBlock(friendsList);
            }
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
        }];
    });
}

- (void)friendsSearchWithQuery:(NSString *)query
                         count:(NSString *)count
                        offset:(NSString *)offset
                    completion: (void (^)(NSArray *items))completionBlock {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        VKRequest * request = [VKApi requestWithMethod:kFriendsGetWithQMethod andParameters:[NSDictionary dictionaryWithObjectsAndKeys:self.userID,kUsersID,query,@"q",count,kCountChats,offset,kOffsetParametr, kFriendsFields,kFriendsFieldsParametr,kNameCase,kNameCaseParametr,nil]];
        [request executeWithResultBlock:^(VKResponse *response) {
            completionBlock(response.json[kItems]);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
        }];
    });
}

- (void)friendsGetOnlineWithCompletion: (void (^)(NSArray *items))completionBlock {
//    if(!self.friends)
//    {
//        [self friendsGetWithCompletion:^(NSMutableArray *items) {
//            NSPredicate *bPredicate;
//            bPredicate = [NSPredicate predicateWithFormat:@"online == 1"];
//            NSArray *filteredArray = [self.friends filteredArrayUsingPredicate:bPredicate];
//            completionBlock(filteredArray);
//            
//        }];
//    } else
//    {
//        NSPredicate *bPredicate;
//        bPredicate = [NSPredicate predicateWithFormat:@"online == 1"];
//        NSArray *filteredArray = [self.friends filteredArrayUsingPredicate:bPredicate];
//        completionBlock(filteredArray);
//        
//    }
}

#pragma mark - Photos





#pragma mark - Video





#pragma mark - Messages


- (void)getDialogsWithOffset:(NSString *)offset
                       count:(NSString *)count
              startMessageID:(NSString *)startMessageID
                 needsUnread:(NSString *)needsUnread
              needsImportant:(NSString *)needsImportant
             needsUnAnswered:(NSString *)needsUnAnswered
                  completion:(void (^)(NSArray * items,NSInteger count,NSInteger unreadCount)) completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:offset,kOffsetParametr,count,kCountChatsParametr,kStartMessageIdParametr,startMessageID,kUnreadParametr,needsUnread,kImportantParametr,needsImportant,kUnasweredParametr,needsUnAnswered, nil];
        VKRequest *postReq = [VKApi requestWithMethod:kGetDialogsMethod andParameters:param];
        [postReq executeWithResultBlock:^(VKResponse *response) {
            NSInteger unreadCount = 0;
            if(response.json[kUnreadDialogs])
            {
                unreadCount = [response.json[kUnreadDialogs] integerValue];
            }
            completionBlock(response.json[kItems],[response.json[kCountChats]integerValue],unreadCount);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
        }];
    });
}

+ (void)messagesGetDialogWithUserId:(NSString *)userId
                             fields:(NSString *)fields
                         completion:(void (^)(NSDictionary * json))completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:@"0",kOffsetParametr,@"1",kCountChatsParametr, nil];
        VKRequest *postReq = [VKApi requestWithMethod:kGetDialogsMethod andParameters:param];
        [postReq executeWithResultBlock:^(VKResponse *response) {
            NSInteger unreadCount = 0;
            if(response.json[kUnreadDialogs])
            {
                unreadCount = [response.json[kUnreadDialogs] integerValue];
            }
            NSArray *items = [NSArray arrayWithArray:response.json];
            completionBlock(items.firstObject);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
        }];
    });
}

+ (void)messagesGetChatWithChatId:(NSString *)chatId
                           fields:(NSString *)fields
                       completion:(void (^)(NSDictionary * json))completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:chatId,@"chat_id",fields,kFields, nil];
        VKRequest *postReq = [VKApi requestWithMethod:kGetChatMethod andParameters:param];
        [postReq executeWithResultBlock:^(VKResponse *response) {
            NSInteger unreadCount = 0;
            if(response.json[kUnreadDialogs])
            {
                unreadCount = [response.json[kUnreadDialogs] integerValue];
            }
            completionBlock(response.json[@"users"]);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);

        }];
    });
}

- (void)messagesDeleteDialogWithUser:(NSString *) userId
                              peerId:(NSString*) peerId
                          completion: (void (^)())completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        VKRequest * request = [VKApi requestWithMethod:kMessagesDeleteDialog andParameters:[NSDictionary dictionaryWithObjectsAndKeys:userId,kUsersID,peerId,kPeerId, nil]];
        [request executeWithResultBlock:^(VKResponse *response) {
            completionBlock();
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
        }];
    });
}

- (void)messagesSearchDialogsWithQ:(NSString *) q
                             limit:(NSString *) limit
                        completion: (void (^)(NSArray * items))completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        VKRequest * request = [VKApi requestWithMethod:kMessagesSearchDialogs andParameters:[NSDictionary dictionaryWithObjectsAndKeys:q,@"q",limit,kLimit,@"photo_50,photo_100",kFields, nil]];
        [request executeWithResultBlock:^(VKResponse *response) {
            completionBlock(response.json);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
        }];
    });
}

+ (void)getHistoryWithOffset:(NSString *)offset
                       count:(NSString *)count
                      userId:(NSString *)userId
                      peerId:(NSString *)peerId
              startMessageId:(NSString *)startMessageId
                         rev:(NSString *)rev
                  completion:(void (^)(NSDictionary * item)) completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:offset,kOffsetParametr,count,kCountChatsParametr,userId,kUsersID,peerId,kPeerId,rev,@"rev", nil];
        VKRequest *postReq = [VKApi requestWithMethod:kMessagesGetHistory andParameters:param];
        [postReq executeWithResultBlock:^(VKResponse *response) {
            completionBlock(response.json);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
        }];
    });
}

+ (void)messagesGetWithIds:(NSArray *) ids
             previewLength:(NSString *) length
                completion:(void (^)(NSDictionary * item)) completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:ids,kMessageIdsParametr,length,kPreviewLengthParametr, nil];
        VKRequest *postReq = [VKApi requestWithMethod:kGetMessageByIdMethod andParameters:param];
        [postReq executeWithResultBlock:^(VKResponse *response) {
            NSArray *items = [NSArray arrayWithArray:response.json[kItems]];
            completionBlock(items.firstObject);
        } errorBlock:^(NSError *error) {
            NSLog(@"%@ repeating", error);
        }];
    });
}


#pragma mark - Images


+ (void)imageWithURL:(NSURL *)url
             success:(void (^)(UIImage *image))success
             failure:(void (^)(NSError *error))failure {
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                       downloadTaskWithURL:url
                                                       completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                           if (error)
                                                           {
                                                               NSLog(@"%@",error);
                                                               return failure(error);
                                                           }
                                                           if (response)
                                                           {
                                                               UIImage *avatar = [UIImage imageWithData:
                                                                                  [NSData dataWithContentsOfURL:location]];
                                                               success(avatar);
                                                           }
                                                       }];
        
        [downloadPhotoTask resume];
    });
}


#pragma mark - VKSdkDelegate


- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    if (result.token)
    {
        
        [self setupAppSettingsWithCompletion:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"authFinished" object:nil];
        }];
        
    }
    else
    {
        if (result.error)
        {
             NSLog(@"%@", result.error);
        }
    }
}

-(void)vkSdkUserAuthorizationFailed {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"networkFailed" object:nil];
}


#pragma mark - User


- (void)createCurrentUserObjectWithUserId:(NSNumber *)userId completion: (void (^)(NSDictionary *json))completionBlock {
    VKRequest *userRequest = [[VKApi users] get:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@", userId], @"user_ids", @"photo_50,photo_100,photo_200,photo_400_orig", @"fields", nil]];
    [userRequest executeWithResultBlock:^(VKResponse *response) {
        NSDictionary *json = [NSArray arrayWithArray: response.json].firstObject;
        self.userID = json[kUserId];
        completionBlock(json);
    } errorBlock:^(NSError *error) {
        NSLog(@"%@ repeating", error);
    }];
    
}


#pragma mark - Users


+ (void)usersGetWithUserID:(NSArray *) userID
                completion: (void (^)(NSArray *users))completionBlock {
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString * result = [[userID valueForKey:@"description"] componentsJoinedByString:@","];
        VKRequest *userRequest = [[VKApi users] get:[NSDictionary dictionaryWithObjectsAndKeys:result, @"user_ids",@"last_seen,online,photo_100, photo_50, photo_200", @"fields", nil]];
        [userRequest executeWithResultBlock:^(VKResponse *response) {
            NSArray *users = [NSArray arrayWithArray:response.json];
            completionBlock(users);
        } errorBlock:^(NSError *error) {
             NSLog(@"%@ repeating", error);
           // [error.vkError.request repeat];
        }];
     });
}


#pragma mark - Reach


- (void)chekReachable {
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    
    NetworkStatus remoteHostStatus = [self.reach currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        NSLog(@"no");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"networkFailed" object:nil];
        self.isConnect = NO;
    }
    else
    {
        if (remoteHostStatus == ReachableViaWiFi)
        {
            NSLog(@"wifi");
            self.isConnect = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"networkGood" object:nil];
        }
        else
        {
            if (remoteHostStatus == ReachableViaWWAN)
            {
                NSLog(@"cell");
                self.isConnect = YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"networkGood" object:nil];
            }
        }
    }
}



#pragma mark - Time


+ (NSString *)dateFormatter:(NSString *) date {
    double unixTimeStamp = [date doubleValue];
    NSTimeInterval interval = unixTimeStamp;
    NSDate *dateOf = [NSDate dateWithTimeIntervalSince1970:interval];
    NSInteger path = [dateOf timeIntervalSinceNow];
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@" H:mm"];
    if (labs(path) >  86400)
    {
        if (labs(path) > (2 * 86400))
        {
            
            [formatter setDateFormat:@"MMM d"];
        }
        else
        {
            return NSLocalizedString(@"yesterday",nil);
            
        }
    }
    [formatter setLocale:[NSLocale currentLocale]];
    
    return  [formatter stringFromDate:dateOf];
}


@end

