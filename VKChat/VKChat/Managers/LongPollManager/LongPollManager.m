//
//  LongPollManager.m
//  VKChat
//
//  Created by Valery Efimov on 14.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import "LongPollManager.h"
#import "LongPollsActions.h"


//get server
static NSString * const kGetLongPollServerMethod =  @"messages.getLongPollServer";
static NSString * const kVKEndpoint =               @"https://api.vk.com/method/";
static NSString * const kGetType =                  @"GET";
static NSString * const kSessionToken =             @"access_token";
static NSString * const kFailed =                   @"failed";
static NSString * const kError =                    @"error";
static NSString * const kErrorCode =                @"error_code";


//update server
static NSString * const kKey =                      @"key";
static NSString * const kServer =                   @"server";
static NSString * const kTs =                       @"ts";
static NSString * const kPts =                      @"pts";
static NSString * const kUpdates =                  @"updates";
static NSString * const kResponse =                 @"response";



@interface LongPollManager ()

@property(strong,nonatomic) NSString *accessToken;
@property(strong,nonatomic) NSString *currentTs;
@property(strong,nonatomic) NSString *currentPts;
@property(strong,nonatomic) NSString *currentServer;
@property(strong,nonatomic) NSString *currentKey;
@property(strong,nonatomic) NSMutableArray *currentUpdates;
@property(assign,nonatomic) BOOL needUpdates;
@property(assign,nonatomic) NSInteger requestCount;

@end

@implementation LongPollManager


#pragma mark - LifeCycle


- (instancetype)initWithAccessToken:(NSString *) accessToken {
    self = [super init];
    
    if (self)
    {
        self.accessToken = accessToken;
        self.needUpdates = YES;
        self.requestCount = 0;
        [self setupSession];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [self longPollServerSetupWithCompletion:^{ }];
        });
        
    }
    return self;
}

- (void)setupSession {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.session = [NSURLSession sessionWithConfiguration:configuration
                                              delegate:self
                                         delegateQueue:nil];
}


#pragma mark - GetLongPollServer


- (NSMutableURLRequest *) setupGetLongPollServerRequest {
    
    NSString *urlString =  [NSString stringWithFormat:@"%@%@?%@=%@",kVKEndpoint,kGetLongPollServerMethod,kSessionToken,self.accessToken];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:26];
    [request setHTTPMethod:kGetType];
    
    return request;
}

- (void)getLongPollServerWithCompletion: (void (^)())completionBlock{
    if(self.requestCount > 3)
    {
        [NSTimer scheduledTimerWithTimeInterval:3.0
                                         target:self
                                       selector:@selector(handleTimer)
                                       userInfo:nil
                                        repeats:NO];
        self.requestCount = 0;
    }
        NSMutableURLRequest * request = [self setupGetLongPollServerRequest];
        NSURLSessionDataTask *getDataTask = [self.session dataTaskWithRequest:request
                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                NSMutableDictionary * json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                             options:kNilOptions
                                                                                                                               error:nil];
                                                                
                                                                if (json[kError][kErrorCode])
                                                                {
                                                                    self.requestCount ++;
                                                                    [self getLongPollServerWithCompletion:^{}];
                                                                }
                                                                else
                                                                {
                                                                    self.currentKey = json[kResponse][kKey];
                                                                    self.currentServer = json[kResponse][kServer];
                                                                    self.currentTs = json[kResponse][kTs];
                                                                    completionBlock();
                                                                }
                                                                
                                                            }];
  
        [getDataTask resume];
  
}

- (void)handleTimer {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"networkFailed"
                                                        object:nil];
}


#pragma mark - LongPollServerUpdate


- (NSMutableURLRequest *) setupLongPollServerUpdateRequestWithServer:(NSString *)server
                                                                  ts:(NSString *)ts
                                                                 key:(NSString *)key {
    self.currentTs = ts;
    NSString *urlString =  [NSString stringWithFormat:@"https://%@?act=a_check&key=%@&ts=%@&wait=25&mode=106&version=1",server,key,self.currentTs];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:30];
    [request setHTTPMethod:kGetType];
    
    return request;
}

- (void)longPollServerUpdateWithServer:(NSString *)server
                                    ts:(NSString *)ts
                                   key:(NSString *)key {
    
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableURLRequest * request = [self setupLongPollServerUpdateRequestWithServer:server ts:ts key:key];
        NSURLSessionDataTask *getDataTask = [self.session dataTaskWithRequest:request
                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                if (!data)
                                                                {
                                                                    [self longPollServerUpdateWithServer:self.currentServer
                                                                                                      ts:self.currentTs
                                                                                                     key:self.currentKey];
                                                                }
                                                                else
                                                                {
                                                                NSMutableDictionary * json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                             options:kNilOptions
                                                                                                                               error:nil];
                                                                if (json[kFailed])
                                                                {
                                                                    NSInteger expression = [json[kFailed] integerValue];
                                                                    switch (expression)
                                                                    {
                                                                        case 1:
                                                                            self.currentTs = json[kTs];
                                                                            break;
                                                                        default:
                                                                            [self getLongPollServerWithCompletion:^{}];
                                                                            break;
                                                                    }
                                                                }
                                                                [self updateServerActionsWithJson:json completion:^{
                                                                    [self longPollServerUpdateWithServer:self.currentServer
                                                                                                      ts:self.currentTs
                                                                                                     key:self.currentKey];
                                                                }];
                                                                }
                                                            }];
        [getDataTask resume];
    });
    
}

- (void)updateServerActionsWithJson:(NSDictionary *) json completion:(void (^)())completionBlock {
    self.currentTs = json[kTs];
    self.currentPts = json[kPts];
    self.currentUpdates = json[kUpdates];
    
    NSNotificationCenter * defaultCenter = [NSNotificationCenter defaultCenter];
    for (int i = 0; i < self.currentUpdates.count; i ++)
    {
        NSInteger expression = [[self.currentUpdates objectAtIndex:i][0] integerValue];
        NSLog(@"event,%li",(long)expression);
        switch (expression) {
            case 1:
                //Замена флагов сообщения
                 [defaultCenter postNotificationName:kReplacementOfMessageFlags
                                                                     object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 2:
                //Установка флагов сообщения
                [defaultCenter postNotificationName:kSettingTheMessageFlags
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 3:
                //Сброс флагов сообщения
                [defaultCenter postNotificationName:kResetTheMessageFlags
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 4:
                //добавление нового сообщения
                [defaultCenter postNotificationName:kAddingNewMessage
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 6:
                //Прочтение всех входящих сообщений
                [defaultCenter postNotificationName:kReadingAllIncomingMessages
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 7:
                //Прочтение всех исходящих сообщений
                [defaultCenter postNotificationName:kReadingAllOutboundMessages
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 8:
                //Друг $user_id стал онлайн.
                [defaultCenter postNotificationName:kFriendBecameOnline
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 9:
                //Друг $user_id стал оффлайн.
                [defaultCenter postNotificationName:kFriendBecameOffline
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 10:
                //Сброс флагов диалога $peer_id.
                [defaultCenter postNotificationName:kResetDialogFlags
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 11:
                //Замена флагов диалога $peer_id.
                [defaultCenter postNotificationName:kReplacementDialogFlags
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 12:
                //Установка флагов диалога $peer_id.
                [defaultCenter postNotificationName:kSettingDialogFlags
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 51:
                //Один из параметров (состав, тема) беседы $chat_id были изменены.
                [defaultCenter postNotificationName:kParametersOfCompositionOrThemeHaveChanged
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 61:
                //Пользователь $user_id начал набирать текст в диалоге.
                [defaultCenter postNotificationName:kUserStartedTypingInDialog
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 62:
                //Пользователь $user_id начал набирать текст в беседе $chat_id.
                [defaultCenter postNotificationName:kUserStartedTypingInChat
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 70:
                //Пользователь $user_id совершил звонок имеющий идентификатор $call_id.
                [defaultCenter postNotificationName:kUserMakesACall
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 80:
                //Новый счетчик непрочитанных в левом меню стал равен $count.
                [defaultCenter postNotificationName:kUpdateUnreadCount
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
            case 114:
                //Изменились настройки оповещений.
                [defaultCenter postNotificationName:kNotificationSettingsHaveChanged
                                                                    object:[self.currentUpdates objectAtIndex:i]];
                break;
        }
    }
    completionBlock();
}

- (void)longPollServerSetupWithCompletion:(void (^)())completionBlock {
        [self getLongPollServerWithCompletion:^{
                [self longPollServerUpdateWithServer:self.currentServer ts:self.currentTs key:self.currentKey];
            
        }];
}


#pragma mark - Decode Flags


+ (NSArray *)decodeFlags:(NSInteger)flags {
    NSString *str = @"";
    for (NSUInteger i = 0; i < 8 ; i++) {
        // Prepend "0" or "1", depending on the bit
        str = [NSString stringWithFormat:@"%@%@",
               flags & 1 ? @"1" : @"0", str];
        flags >>= 1;
    }
    NSMutableArray *chars = [[NSMutableArray alloc] initWithCapacity:[str length]];
    for (int i=0; i < [str length]; i++) {
        NSString *ichar  = [NSString stringWithFormat:@"%C", [str characterAtIndex:i]];
        [chars addObject:ichar];
    }
    chars = [[chars reverseObjectEnumerator]allObjects].mutableCopy;
    NSMutableArray *flagsArray = [NSMutableArray new];
    NSArray *powers = [[NSArray alloc] initWithObjects:@(1),@(2),@(4),@(8),@(16),@(32),@(64),@(128),@(256),@(512),nil];
    for (int i = 0 ; i < chars.count; i ++)
    {
        if ([chars[i]  isEqual: @"1"])
        {
            [flagsArray addObject:powers[i]];
        }
       
    }
    return (NSArray*)flagsArray;
}


@end
