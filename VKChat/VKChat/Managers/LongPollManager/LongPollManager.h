//
//  LongPollManager.h
//  VKChat
//
//  Created by Valery Efimov on 14.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LongPollManager : NSObject<NSURLSessionDelegate>

@property(strong,nonatomic) NSURLSession *session;

- (instancetype)initWithAccessToken:(NSString *)accessToken;

+ (NSArray *)decodeFlags:(NSInteger)flags;


@end
