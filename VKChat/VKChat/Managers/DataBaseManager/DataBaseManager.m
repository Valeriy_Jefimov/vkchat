//
//  DataBaseManager.m
//  VKChat
//
//  Created by Valery Efimov on 16.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "DataBaseManager.h"
#import "VKManagerConstants.h"
#import "VKManager.h"



@interface DataBaseManager ()

@property (strong,nonatomic,readwrite) NSManagedObjectContext *mainManagedObjectContext;


@property (strong,nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong,nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end


@implementation DataBaseManager


#pragma mark - LifeCycle


+ (instancetype)sharedInstance {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    
    if (self)
    {
        [self setupCoreDataStack];
    }
    
    return self;
}

#pragma mark - CoreData


- (void)setupCoreDataStack {
    
    [self setupManagedObjectModel];
    [self setupPersistentStoreCoordinator];
    [self addMainContext];
}

- (void)setupManagedObjectModel {
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource: @"DialogsModel"
                                              withExtension: @"momd"];
    
    self.managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
}

- (void)setupPersistentStoreCoordinator {
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent: @"DialogsModel.sqlite"];
    
    NSDictionary *option = @{NSMigratePersistentStoresAutomaticallyOption  : @YES,
                             NSInferMappingModelAutomaticallyOption      : @YES};
    NSError *error = nil;
    self.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    if (![self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                       configuration:nil
                                                                 URL:storeURL
                                                             options:option
                                                               error:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

- (void)addMainContext {
    
    NSManagedObjectContext *managedObjectContext = [[NSManagedObjectContext alloc]
                                                    initWithConcurrencyType:NSMainQueueConcurrencyType];
    
    self.mainManagedObjectContext = managedObjectContext;
    [self.mainManagedObjectContext setPersistentStoreCoordinator: self.persistentStoreCoordinator];
}

- (NSManagedObjectContext *)childManagedObjectContext {
    
    NSManagedObjectContext *managedObjectContext = [[NSManagedObjectContext alloc]
                                                    initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    
    managedObjectContext.parentContext = _mainManagedObjectContext;
    return managedObjectContext;
}


#pragma mark - Application's Documents directory


- (NSURL *)applicationDocumentsDirectory {
    
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}


#pragma mark - Basic Actions


+ (UIImage *)imagefromDocumentsDirectoryWithName:(NSString *) fileName {
    NSString *str = [NSString stringWithFormat:@"%@.png",fileName];
    NSString *fullImgName = [[DataBaseManager documentsDirectory] stringByAppendingPathComponent:[NSString stringWithString:str]];
    return [UIImage imageWithContentsOfFile:fullImgName];
}

+ (NSString *)saveToDocumentsImageWithImage:(UIImage *) image
                                  imageName:(NSString *) imageName {
    NSData *imageData = UIImagePNGRepresentation(image);
    NSString *imageUnicName = [NSString stringWithFormat:@"/%@.png",imageName];
    NSString *imagePath = [[DataBaseManager documentsDirectory] stringByAppendingPathComponent:imageUnicName];
    
    if (![imageData writeToFile:imagePath atomically:NO])
    {
        NSLog(@"Failed to cache image data to disk");
    }
    else
    {
        NSLog(@"the cachedImagedPath is %@",imagePath);
    }
    
    return imageUnicName;
}

+ (void)removeImageFromDocumentsWithName:(NSString *) name {
    NSString *imageUnicPath = [NSString stringWithFormat:@"%@/%@",[DataBaseManager documentsDirectory],name];
    [[NSFileManager defaultManager] removeItemAtPath: imageUnicPath error: nil];
}

+ (NSString *)documentsDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}


#pragma mark - Save Contexts


- (void)saveContext:(NSManagedObjectContext*) context {
    
    NSError* error = nil;
    [self saveDatabaseForContext: context
                           error: error];
}

- (void)saveDatabaseForContext:(NSManagedObjectContext*) context
                         error:(NSError*) error
{
    if (!self.persistentStoreCoordinator)
    {
        return;
    }
    NSManagedObjectContext* __strong strongContext = context;
    [strongContext performBlockAndWait: ^{
        NSError* saveError = nil;
        if ([strongContext save: &saveError])
        {
            NSManagedObjectContext* parentContext = strongContext.parentContext;
            if (parentContext)
            {
                [parentContext performBlockAndWait: ^{
                    NSError* parentSaveError = nil;
                    [self saveDatabaseForContext: parentContext
                                           error: parentSaveError];
                    
                    if (parentSaveError)
                    {
                        NSLog(@"Context save error");
                    }
                }];
            }
        }
    }];
}


#pragma mark -  Chats


- (void)deleteAllChats {
    NSFetchRequest *allChats = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context = [self childManagedObjectContext];
    [allChats setEntity:[NSEntityDescription entityForName:@"ChatDBModel"
                                    inManagedObjectContext:context]];
    [allChats setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError *error = nil;
    NSArray *chats = [context executeFetchRequest:allChats error:&error];
    for (NSManagedObject *chat in chats)
    {
        [self.childManagedObjectContext deleteObject:chat];
    }
    [self saveContext:context];
}


- (void)saveChatsWithItems:(NSArray *) items
             dispatchGroup:(dispatch_group_t) group
                   context:(NSManagedObjectContext *) childContext {
    for (NSDictionary * dict  in items)
    {
        NSString *chatID = [dict[kMessage][@"chat_id"] stringValue];
        if(!chatID)
        {
            chatID = [dict[kMessage][kUserID] stringValue];
        }
        ChatDBModel *newChat = [self messagesGetChatWithChatId:chatID context:childContext];
        NSMutableSet *usersIds = [NSMutableSet new];
        [usersIds addObjectsFromArray:[NSArray arrayWithArray:dict[kMessage][@"chat_active"]]];
        if (usersIds.count < 1)
        {
            [usersIds addObjectsFromArray:[NSArray arrayWithObject:dict[kMessage][kUsersID]]];
        }
        if (!newChat)
        {
            newChat = [ChatDBModel newEntityWithContext:childContext];
            NSLog(@"chat  %@ create",chatID);
            [newChat setupWithJson:dict completion:^{}];
            [self setupUsersWithUsersSet:usersIds chat:newChat dataDictionary:dict context:childContext dispatchGroup:group];
        }
        else
        {
            if(!([newChat.time    isEqualToString:[VKManager dateFormatter:dict[kMessage][@"date"]]]      &&
                 [newChat.body    isEqualToString:dict[kMessage][@"body"]]                                &&
                 [newChat.userID  isEqualToString:dict[kMessage][kUserId]]))
            {
                NSLog(@"chat  %@ update",chatID);
                [self setupUsersWithUsersSet:usersIds chat:newChat dataDictionary:dict context:childContext dispatchGroup:group];
            }
        }
    }
}

- (void)setupUsersWithUsersSet:(NSSet *) usersIds
                          chat:(ChatDBModel *) newChat
                dataDictionary:(NSDictionary *) dict
                       context:(NSManagedObjectContext *) childContext
                 dispatchGroup:(dispatch_group_t) group {
    [newChat setupWithJson:dict completion:^{ }];
    NSArray *usersIdsArray = [usersIds allObjects];
    dispatch_group_enter(group);
    [newChat getUsersWithUserIDs:usersIdsArray context:childContext completion:^{
        dispatch_group_leave(group);
    }];
}

- (ChatDBModel *)messagesGetChatWithChatId:(NSString *) chatId
                                   context:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"ChatDBModel" inManagedObjectContext:context];
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES], nil];
    if(chatId)
    {
        request.predicate = [NSPredicate predicateWithFormat:@"chatID == %@ ",chatId];
    }
    request.fetchLimit = 30;
    NSError *error = nil;
    NSArray *fetchedResults = [context executeFetchRequest:request error:&error];
    
    if([fetchedResults count] >= 1)
    {
        if (fetchedResults.count > 1)
        {
            for (int i = 1; i < fetchedResults.count ; i ++)
            {
                [context deleteObject:fetchedResults[i]];
            }
        }
        return [fetchedResults objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}

- (void)messagesDeleteChatWithChatId:(NSString *) chatId {
    NSManagedObjectContext *context = [self childManagedObjectContext];
    ChatDBModel *chat = [self messagesGetChatWithChatId:chatId context:context];
    NSManagedObjectID *moId = [chat objectID];
    [self.childManagedObjectContext deleteObject:[context objectWithID:moId]];
    [self saveContext:context];
}

- (void)messagesDeleteChatWithChat:(ChatDBModel *) chat {
    NSManagedObjectContext *context = [self childManagedObjectContext];
    [context deleteObject:chat];
    [self saveContext:context];
}


#pragma mark - Messages


- (void)messagesSaveMessageWithMessageModel:(NSDictionary *) json {
    NSManagedObjectContext *context = [self childManagedObjectContext];
    MessageDBModel *newMessage = [NSEntityDescription insertNewObjectForEntityForName:@"MessageDBModel"
                                                               inManagedObjectContext:context];
    [newMessage setupWithJson:json context:context];
    [self saveContext:context];
}

- (MessageDBModel *)messagesGetWithMessageId:(NSString*) messageId context:(NSManagedObjectContext *) context {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"MessageDBModel"
                                 inManagedObjectContext:context];
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"messageID" ascending:YES], nil];
    request.predicate = [NSPredicate predicateWithFormat:@"messageID == %@",messageId];
    NSError *error = nil;
    NSArray *fetchedResults = [context executeFetchRequest:request error:&error];
    if(fetchedResults.count > 0)
    {
        return fetchedResults[0];
    }
    return nil;
}

- (void)messagesDeleteMessageWithMessageID:(NSString *) messageId {
    NSManagedObjectContext *context = [[DataBaseManager sharedInstance] childManagedObjectContext];
    NSManagedObjectID *moId = [[self messagesGetWithMessageId:messageId context:context] objectID];
    [context deleteObject:[context objectWithID:moId]];
    [self saveContext:context];
}



#pragma mark - Users

+ (void)usersCreateNewCurrentUserWithDictionary:(NSDictionary *) dictionary {
    NSManagedObjectContext *context = [[DataBaseManager sharedInstance] childManagedObjectContext];
    CurrentUserDBModel *newCurrentUser = [CurrentUserDBModel newEntityWithContext:context];
    [newCurrentUser setupWithJson:dictionary context:context];
    [[DataBaseManager sharedInstance] saveContext:context];
}

+ (CurrentUserDBModel *)usersGetCurrentUserInContext:(NSManagedObjectContext *) context {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"CurrentUserDBModel" inManagedObjectContext:context];
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"userID" ascending:YES], nil];
    request.fetchLimit = 1;
    NSError *error = nil;
    NSArray *fetchedResults;
    @synchronized([DataBaseManager sharedInstance].persistentStoreCoordinator) {
        fetchedResults = [context executeFetchRequest:request error:&error];
    }
    if(fetchedResults.count > 0)
    {
      __strong  CurrentUserDBModel *user = fetchedResults [0];
        return user;
    }
    else
    {
        return nil;
    }

}

- (void)usersSaveUserWithUserModel:(NSDictionary *) json {
    NSManagedObjectContext *context = [[DataBaseManager sharedInstance] childManagedObjectContext];
    UserDBModel *newUser = [UserDBModel newEntityWithContext: context];
    [newUser setupWithJson:json context: context];
    [self saveContext:context];
}

- (UserDBModel *)usersGetWithUserId:(NSString*)userID context:(NSManagedObjectContext *) context {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"UserDBModel" inManagedObjectContext:context];
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"userID" ascending:YES], nil];
    request.predicate = [NSPredicate predicateWithFormat:@"userID == %@",userID];
    request.fetchLimit = 20;
    NSError *error = nil;
    NSArray *fetchedResults;
    @synchronized(self.persistentStoreCoordinator) {
        fetchedResults = [context executeFetchRequest:request error:&error];
    }
    if(fetchedResults.count > 0)
    {
        return fetchedResults [0];
    }
    else
    {
        return nil;
    }
}

- (void)usersDeleteUserWithUserID:(NSString *) userId {
    NSManagedObjectContext *context = [[DataBaseManager sharedInstance] childManagedObjectContext];
    NSManagedObjectID *moId = [[self usersGetWithUserId:userId context:context] objectID];
    [self.childManagedObjectContext deleteObject:[context objectWithID:moId]];
    [self saveContext:context];
}



#pragma mark - Comparing


+ (BOOL)isUnique:(NSManagedObject *) object inContext:(NSManagedObjectContext *) context {
    NSMutableString *format = [NSMutableString string];
    NSEntityDescription *description = object.entity;
    NSArray *attributes = [[description attributesByName] allKeys];
    
    for(NSString *attr in attributes) {
        if(format.length)
            [format appendString:@" AND "];
        [format appendFormat:@"%@==%@", attr, [object valueForKey:attr]];
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:format];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:description.name];
    fetchRequest.predicate = predicate;
    if([context countForFetchRequest:fetchRequest error: nil] == 0)
    {
        return YES;
    }
    return NO;
    
}


@end
