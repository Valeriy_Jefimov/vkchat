//
//  DataBaseManager.h
//  VKChat
//
//  Created by Valery Efimov on 16.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDBModel+CoreDataClass.h"
#import "MessageDBModel+CoreDataClass.h"
#import "ChatDBModel+CoreDataProperties.h"
#import "CurrentUserDBModel+CoreDataClass.h"
#import <UIKit/UIKit.h>



@interface DataBaseManager : NSObject

@property (strong,nonatomic,readonly) NSManagedObjectContext *mainManagedObjectContext;


+ (instancetype)sharedInstance;
- (NSManagedObjectContext *)childManagedObjectContext;
#pragma mark -  Chats


- (void)saveChatsWithItems:(NSArray *) items dispatchGroup:(dispatch_group_t) group context:(NSManagedObjectContext *) context;
- (ChatDBModel *)messagesGetChatWithChatId:(NSString *) chatId
                                   context:(NSManagedObjectContext *)context;
- (void)messagesDeleteChatWithChatId:(NSString *) chatId;
- (void)deleteAllChats;


#pragma mark - Messages


- (void)messagesSaveMessageWithMessageModel:(NSDictionary *) json;
- (MessageDBModel *)messagesGetWithMessageId:(NSString*) messageId
                                    context:(NSManagedObjectContext *) context ;
- (void)messagesDeleteMessageWithMessageID:(NSString *) messageId;


#pragma mark - Users


+ (void)usersCreateNewCurrentUserWithDictionary:(NSDictionary *) dictionary;
+ (CurrentUserDBModel *)usersGetCurrentUserInContext:(NSManagedObjectContext *) context;

- (void)usersSaveUserWithUserModel:(NSDictionary *) json;
- (UserDBModel *)usersGetWithUserId:(NSString*)userID context:(NSManagedObjectContext *) context;
- (void)usersDeleteUserWithUserID:(NSString *) userId;


- (void)saveContext: (NSManagedObjectContext*) context;


#pragma mark - Images


+ (UIImage *)imagefromDocumentsDirectoryWithName:(NSString *) fileName;
+ (NSString *)saveToDocumentsImageWithImage:(UIImage *) image
                                  imageName:(NSString *) imageName;
+ (void)removeImageFromDocumentsWithName:(NSString *) name;


#pragma mark - Comparing


+ (BOOL)isUnique:(NSManagedObject *) object inContext:(NSManagedObjectContext *) context;

@end
