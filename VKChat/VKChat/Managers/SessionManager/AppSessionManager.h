//
//  AppSessionManager.h
//  VKChat
//
//  Created by Valery Efimov on 17.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VKManager.h"
#import "DataBaseManager.h"

@protocol ChatListControllerProtocol <NSObject>

- (void)configureNavigationBarState:(NSString *) state;
- (void)reloadTable;

@end

@interface AppSessionManager : NSObject

//managers
@property(strong,nonatomic) VKManager *vkManager;
@property(strong,nonatomic) DataBaseManager *dataBaseManager;
@property(strong,nonatomic) NSString *userImageUrl;

@property(strong,nonatomic) id<ChatListControllerProtocol> chatListControllerDelegate;

- (void)chekReachable;


#pragma mark - DataBaseanager


-(void)saveContext:(NSManagedObjectContext*) context;


#pragma mark - FetchedResultsController


- (NSManagedObjectContext *)parentContext;
- (NSManagedObjectContext *)childContext;


#pragma mark - LoginScreen

- (void)setupDataForChatsViewControllerWithCompletion: (void (^)(NSInteger dataCount,NSArray * data))completionBlock;


//
//+ (void)imageWithURL:(NSURL *)url
//             success:(void (^)(UIImage *image))success
//             failure:(void (^)(NSError *error))failure;
//
////Audio
//- (void)audioGetWithCount:(NSString *)count
//                   offset:(NSString *)offsset
//               completion: (void (^)(NSArray * items,NSInteger count))completionBlock;
//
//- (void)audioGetById:(NSString *)audioId
//          completion: (void (^)(VKAudio * audio))completionBlock;
//
////Docs
//- (void)docsAddWithID:(NSArray *)docIDs
//            accessKey:(NSString *)accessKey
//           completion: (void (^)(VKDocs *doc))completionBlock;
//
//- (void)docsGetWithCount:(NSString *)count
//                  offset:(NSString *)offset
//                    type:(NSString *)type
//              completion: (void (^)(NSArray * items,NSInteger count))completionBlock;
//
//- (void)docsUpload:(NSString *)doc withTitle:(NSString *)title
//        completion:(void (^)(VKDocs * doc))completionBlock;
//- (void)deleteWithID:(NSString *)docID;
//
//- (void)login;
//- (void)autorize;
//
////Account
//+ (void)registreDeviceWithDeviceToken:(NSData*)deviceToken;
//- (void)setOffline;
//- (void)setOnline;
//- (void)setPushSettings;
//- (void)setSilenceMode;
//- (void)unregisterDevice;
//
////Users
//+ (void)usersGetWithUserID:(NSString*)userID
//                completion: (void (^)(NSDictionary *user))completionBlock;

@end
