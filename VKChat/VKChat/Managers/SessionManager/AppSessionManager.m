//
//  AppSessionManager.m
//  VKChat
//
//  Created by Valery Efimov on 17.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "AppSessionManager.h"


@interface AppSessionManager ()



@end


@implementation AppSessionManager

- (instancetype)init {
    self = [super init];
    
    if (self)
    {
        self.vkManager = [[VKManager alloc]init];
        self.dataBaseManager = [DataBaseManager sharedInstance];
    }
    return self;
}


- (void)chekReachable {
    [self.vkManager chekReachable];
}


#pragma mark - DataBaseanager


-(void)saveContext:(NSManagedObjectContext*) context {
    [self.dataBaseManager saveContext: context];
}


#pragma mark - FetchedResultsController


- (NSManagedObjectContext *)parentContext {
    return self.dataBaseManager.mainManagedObjectContext;
}

- (NSManagedObjectContext *)childContext {
    return [self.dataBaseManager childManagedObjectContext];
}


#pragma mark - LoginScreen


- (void)setupDataForChatsViewControllerWithCompletion: (void (^)(NSInteger dataCount,NSArray * data))completionBlock {
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"ChatDBModel"];
    NSArray *results = [self.childContext executeFetchRequest:request error:nil];
    NSLog(@"completion called");
    completionBlock(results.count,results);
    [self.vkManager getDialogsWithOffset:@"0"
                                   count:@"30" startMessageID:@"" needsUnread:@"1" needsImportant:@"0" needsUnAnswered:@"1"
                              completion:^(NSArray *items, NSInteger count, NSInteger unreadCount) {
                                   items = [[items reverseObjectEnumerator] allObjects];
                                  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                      dispatch_group_t group = dispatch_group_create();
                                      __block   NSManagedObjectContext *childContext = [self.dataBaseManager childManagedObjectContext];
                                      [self.dataBaseManager saveChatsWithItems:items dispatchGroup:group context:childContext];
                                      dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                          [self.chatListControllerDelegate configureNavigationBarState:@"Dialogues"];
                                          [self.dataBaseManager saveContext:childContext];
                                          [self.chatListControllerDelegate reloadTable];
                                      });
                                  });
                              }];
    NSLog(@"setup completed method called");
}



@end
