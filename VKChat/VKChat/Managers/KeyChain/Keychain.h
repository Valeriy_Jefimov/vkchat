//
//  KeyChain.h
//  CityBase
//
//  Created by user on 10/31/16.
//  Copyright © 2016 vlaskos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Keychain : NSObject {
    NSString * service;
    NSString * group;
}
-(id) initWithService:(NSString *) service_ withGroup:(NSString*)group_;

-(BOOL) insert:(NSString *)key :(NSData *)data;
-(BOOL) update:(NSString*)key :(NSData*) data;
-(BOOL) remove: (NSString*)key;
-(NSData*) find:(NSString*)key;

+ (NSString*) readFromKeychainWithKey:(NSString*)key;

@end
