//
//  ChatDataSourse.h
//  VKChat
//
//  Created by Valery Efimov on 04.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "BaseTableViewDataSource.h"
#import "ChatDBModel+CoreDataProperties.h"

@interface ChatDataSource : BaseTableViewDataSource <NSFetchedResultsControllerDelegate>

@property(assign,nonatomic) BOOL isPhoto;
@property(strong,nonatomic) UIImage *userImage;
@property(strong,nonatomic) ChatDBModel *chatModel;

@property (strong,nonatomic) NSMutableArray *searchData;
@property (assign,nonatomic) BOOL isSearch;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property(strong,nonatomic) id<TableDelegateProtocol> delegate;

- (instancetype)initWithSessionmanager:(AppSessionManager*)sessionManager chatModel:(ChatDBModel *) model;
- (void)setTableDelegate:(id<TableDelegateProtocol>)aDelegate;

@end
