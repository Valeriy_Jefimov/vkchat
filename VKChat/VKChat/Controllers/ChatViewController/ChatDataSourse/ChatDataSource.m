//
//  ChatDataSourse.m
//  VKChat
//
//  Created by Valery Efimov on 04.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "ChatDataSource.h"
#import "InTableViewCell.h"
#import "OutTableViewCell.h"



@implementation ChatDataSource

- (instancetype)initWithSessionmanager:(AppSessionManager*)sessionManager chatModel:(ChatDBModel *) model  {
    self = [super init];
    if (self)
    {
        self.sessionManager = sessionManager;
        [NSFetchedResultsController deleteCacheWithName:@"Root"];
        self.chatModel = model;
        [self setupFetchedResultsController];
        
    }
    return self;
}

- (void)setTableDelegate:(id<TableDelegateProtocol>)aDelegate {
    self.delegate = aDelegate;
}

#pragma mark - Fetched Results Controller


- (void)setupFetchedResultsController {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"MessageDBModel" inManagedObjectContext:[self.sessionManager parentContext]];
    [fetchRequest setEntity:entity];
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"unixTimeStamp" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"chatID == %@ OR userID == %@",self.chatModel.chatID,self.chatModel.chatID];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:[self.sessionManager parentContext] sectionNameKeyPath:nil
                                                   cacheName:@"Root"];
    self.fetchedResultsController = theFetchedResultsController;
    self.fetchedResultsController.delegate = self;
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self dataCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    InTableViewCell *inCell = (InTableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InTableViewCell class])];
    if (inCell == nil)
    {
        inCell = [[InTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault  reuseIndefier:NSStringFromClass([InTableViewCell class]) ];
    }
    
    OutTableViewCell *outCell = (OutTableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([OutTableViewCell class])];
    if (outCell == nil)
    {
        outCell = [[OutTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault  reuseIndefier:NSStringFromClass([OutTableViewCell class]) ];
    }
    
    MessageDBModel *current = self.fetchedResultsController.fetchedObjects[indexPath.row];
    if([current.outState isEqualToString:@"0"])
    {
        NSManagedObjectContext *context = [[DataBaseManager sharedInstance] childManagedObjectContext];
        UserDBModel *user = [[DataBaseManager sharedInstance]usersGetWithUserId:current.userID context:context];
        inCell.avatar.image = [user userImage];
        [inCell configureUIWithMessage:current];
        return inCell;
    }
    else
    {
        [outCell configureUIWithMessage:current];
        return outCell;
    }
}


- (NSInteger)dataCount {
    return self.fetchedResultsController.fetchedObjects.count;
}


#pragma mark - NSFetchedResultsControllerDelegate


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.delegate beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.delegate insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                         withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.delegate deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                         withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.delegate insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                                 withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.delegate deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                 withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeUpdate: {
            MessageDBModel *message = self.fetchedResultsController.fetchedObjects[indexPath.row];
            if([message.outState isEqualToString:@"0"])
            {
               InTableViewCell *cell = (InTableViewCell *)[self.delegate getCellForRowAtIndexPath:indexPath];
               [cell configureUIWithMessage:message];
            }
            else
            {
                OutTableViewCell *cell = (OutTableViewCell *)[self.delegate getCellForRowAtIndexPath:indexPath];
                [cell configureUIWithMessage:message];
               
            }
        }
            break;
        case NSFetchedResultsChangeMove:
            [self.delegate deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                 withRowAnimation:UITableViewRowAnimationNone];
            [self.delegate insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                                 withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.delegate endUpdates];
}

@end
