//
//  AttacmentCollectionViewDataSource.h
//  VKChat
//
//  Created by Valery Efimov on 04.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AttacmentCollectionViewDataSource : NSObject<UICollectionViewDataSource>

@property(strong,nonatomic) NSMutableArray *data;

@end
