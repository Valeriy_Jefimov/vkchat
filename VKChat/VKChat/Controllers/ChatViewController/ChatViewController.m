//
//  ChatViewController.m
//  VKChat
//
//  Created by Valery Efimov on 04.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "ChatViewController.h"
#import "ChatView.h"
#import "AttacmentCollectionViewDataSource.h"
#import "BaseTableViewCell.h"


static CGFloat  const kOffset =                   30.0;


@interface ChatViewController ()<ChatProtocol,UITextViewDelegate,UICollectionViewDelegate>

@property(strong,nonatomic) ChatView *chatView;
@property(strong,nonatomic) AttacmentCollectionViewDataSource *collectionDataSource;
@property(strong,nonatomic) AppSessionManager *sessionManager;
@property(strong,nonatomic) UIImage *userImage;
@property(assign,nonatomic) CGFloat dataViewWidth;
@property(assign,nonatomic) BOOL shouldScrollToLastRow;



@end

@implementation ChatViewController

#pragma mark - LifeCycle

- (instancetype)initWithChatModel:(ChatDBModel *)model sessionManager:(AppSessionManager *) sessionManager {
    self = [super init];
    if (self)
    {
        self.dataViewWidth = self.chatView.frame.size.width / 3 * 2;
        self.sessionManager = sessionManager;
        self.model = model;
        self.userImage = [UIImage new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.shouldScrollToLastRow = YES;
    self.collectionDataSource = [AttacmentCollectionViewDataSource new];
    
    self.dataSourse = [[ChatDataSource alloc]initWithSessionmanager:self.sessionManager chatModel:self.model ];
    [self.dataSourse setDelegate:self];
    self.dataSourse.dataMaxCount = [self.model.messagesMaxCount integerValue];
    self.dataSourse.userImage = self.userImage;
    
    [self.chatView setTableDelegat:self
                      chatDelegate:self
                    chatDataSource:self.dataSourse
            inputTextFieldDelegate:self attacmentCollectionViewDelegate:self
 attacmentCollectionViewDataSource:self.collectionDataSource];
    self.dataSourse.userImage = self.userImage;
    [self.chatView.tableView reloadData];
    
    
    [self notificationSetup];
}


- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    if (self.shouldScrollToLastRow)
    {
        self.shouldScrollToLastRow = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.chatView scrollToBottom];
        });
    }
}

- (void)setupUserImage:(UIImage*)userImage {
    self.userImage = userImage;
}


- (void)loadView {
    [super loadView];
    
    self.view.backgroundColor = [UIColor colorWithRed:0.90 green:0.91 blue:0.94 alpha:1.0];
    self.chatView = [[ChatView alloc]initWithFrame:self.view.frame];
    self.view = self.chatView;
    [self.chatView setNeedsUpdateConstraints];
    [self.chatView layoutIfNeeded];
    self.shouldScrollToLastRow = YES;
    
    [self navigationBarSetup];
}

- (void)notificationSetup {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(addingNewMessage:)
//                                                     name:kAddingNewMessage object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(userBecomeOnline:)
    //                                                 name:kFriendBecameOnline object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(userBecomeOffline:)
    //                                                 name:kFriendBecameOffline object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(resetMessageFlags:)
    //                                                 name:kResetTheMessageFlags object:nil];
    //
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - UISetup


- (void)navigationBarSetup {
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.22 green:0.41 blue:0.58 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                       style: UIBarButtonItemStylePlain
                                                                      target:self action:@selector(back:)];
    self.navigationItem.leftBarButtonItem = settingsButton;
    
    UIBarButtonItem *newChatButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"chatInfo"]
                                                                      style: UIBarButtonItemStylePlain
                                                                     target:self action:@selector(settingsView:)];
    
    self.navigationItem.rightBarButtonItem = newChatButton;
    self.title = NSLocalizedString(self.model.title,nil);
}


#pragma mark - NavigationBarActions


- (void)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)settingsView:(id)sender {
    
}

#pragma mark - TableDelegateProtocol


- (void)reloadRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths
              withRowAnimation:(UITableViewRowAnimation)animation  {
    [self.chatView.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (UITableViewCell *)getCellForRowAtIndexPath:(NSIndexPath *)indexPat {
    return [self.chatView.tableView cellForRowAtIndexPath:indexPat];
}

- (void)reloadTable {
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.chatView.tableView reloadData];
    });
}

- (void)beginUpdates {
    [self.chatView.tableView beginUpdates];
}

- (void)endUpdates {
    [self.chatView.tableView endUpdates];
}

- (void)insertSections:(NSIndexSet *)sections withRowAnimation:(UITableViewRowAnimation)animation {
    [self.chatView.tableView insertSections:sections withRowAnimation:UITableViewRowAnimationNone];
}

- (void)deleteSections:(NSIndexSet *)sections withRowAnimation:(UITableViewRowAnimation)animation {
    [self.chatView.tableView deleteSections:sections withRowAnimation:UITableViewRowAnimationNone];
}

- (void)insertRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation {
    [self.chatView.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (void)deleteRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation {
    [self.chatView.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - UITableViewDelegate


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageDBModel *message = self.dataSourse.fetchedResultsController.fetchedObjects[indexPath.row];
    CGFloat height = message.bodySizeHeight;
    if([message.attach allObjects].count > 0)
    {
        height += [message.attach allObjects].count * self.dataViewWidth;
    }
    height += kOffset; //for offset
    return height;
}


#pragma mark - UITextViewDelegate


- (void)textViewDidBeginEditing:(UITextView *)textView {
    
}


#pragma mark - UIScrollViewDelegate


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y >= scrollView.contentSize.height) {
        scrollView.contentOffset = CGPointZero;
    }
    
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height)
    {
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, scrollView.contentSize.height - scrollView.frame.size.height)];
    }
}


#pragma mark - ChatProtocol


-(void)loadMoreChatData {
//    if(self.dataSourse.data.count < self.model.messagesMaxCount)
//    {
//        self.chatView.tableView.pullToRefreshView.alpha = 1.0;
//        NSString *peerId = @"";
//        if (self.model.chatID != nil)
//        {
//            NSInteger peer = kPeer + [self.model.chatID integerValue];
//            peerId = [NSString stringWithFormat:@"%ld",(long)peer];
//        }
//        NSString *offset = [NSString stringWithFormat:@"%lu",(unsigned long)self.dataSourse.data.count ];
//        self.chatView.tableView.tableHeaderView.hidden = NO;
//        [self.sessionManager.vkManager getHistoryWithOffset:offset count:@"30" userId:self.model.userID peerId:peerId startMessageId:@"" rev:@"" completion:^(NSDictionary *item) {
//            NSArray *arrayItems = [NSArray arrayWithArray:item[@"items"] ];
//            MessageModel *message;
//            for (NSInteger i = 0; i < arrayItems.count; i++)
//            {
//                message = [[MessageModel alloc]initWithJson:arrayItems[i]];
//                [self.dataSourse.data insertObject:message atIndex:0];
//                NSIndexPath *zero = [NSIndexPath indexPathForRow:0 inSection:0];
//                
//                [UIView setAnimationsEnabled:NO];
//                [self.chatView.tableView beginUpdates];
//                [self.chatView.tableView  insertRowsAtIndexPaths:[NSArray arrayWithObject:zero] withRowAnimation:UITableViewRowAnimationNone];
//                NSIndexPath *position = [NSIndexPath indexPathForRow:i inSection:0];
//                [self.chatView.tableView scrollToRowAtIndexPath:position atScrollPosition:UITableViewScrollPositionTop animated:NO];
//                [self.chatView.tableView  endUpdates];
//                [UIView setAnimationsEnabled:YES];
//            }
//            
//            [self.chatView.tableView.pullToRefreshView stopAnimating];
//        }];
//    }
//    else
//    {
//         self.chatView.tableView.pullToRefreshView.alpha = 0.0;
//        self.chatView.tableView.showsPullToRefresh = NO;
//    }
}


#pragma mark - Notifacations actions


- (void)addingNewMessage:(NSNotification *)notification {
    NSInteger i =[[notification object][3] integerValue];
    if(i < kPeer)
    {
        
        
    }
    else
    {
       
    }
}


#pragma mark -


//- (void)userBecomeOnline:(NSNotification *)notification {
//    ChatDBModel *model = [ChatModel new];
//    NSArray *object = [[NSArray alloc] initWithArray:[notification object]];
//    if([[notification object][2] integerValue] < kPeer)
//    {
//        NSArray *object = [[NSArray alloc] initWithArray:[notification object]];
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID == %d", object[2] ];
//        NSArray *filteredArray = [self.dataSourse.data filteredArrayUsingPredicate:predicate];
//        model = [filteredArray firstObject];
//        if(model)
//        {
//            [self.dataSourse.data removeObject:model];
//            model.userOnline = @"1";
//            [self.dataSourse.data insertObject:model atIndex:0];
//        }
//    }
//    else
//    {
//        NSInteger i = [[NSString stringWithFormat:@"%@",object[2] ] integerValue];
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"chatID == %d", ( i - kPeer)];
//        NSArray *filteredArray = [self.dataSourse.data filteredArrayUsingPredicate:predicate];
//        model = [filteredArray firstObject];
//        if(model)
//        {
//            
//        }
//    }
//}
//
//- (void)userBecomeOffline:(NSNotification *)notification {
//    NSArray *object = [[NSArray alloc] initWithArray:[notification object]];
//    ChatModel *model = [ChatModel new];
//    if([[notification object][2] integerValue] < kPeer)
//    {
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID == %d", [object[2] integerValue]];
//        NSArray *filteredArray = [self.dataSourse.data filteredArrayUsingPredicate:predicate];
//        model = [filteredArray firstObject];
//        if(model)
//        {
//            
//        }
//    }
//    else
//    {
//        NSInteger i = [object[2] integerValue];
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"chatID == %d", ( i - kPeer)];
//        NSArray *filteredArray = [self.dataSourse.data filteredArrayUsingPredicate:predicate];
//        model = [filteredArray firstObject];
//        if(model)
//        {
//            
//        }
//    }
//}
//
//- (void)resetMessageFlags:(NSNotification *)notification {
//    
//}


#pragma mark -


- (void)keyboardWillShow:(NSNotification *)notification {
    [UIView beginAnimations:nil context:nil];
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue]];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    CGRect kbFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.chatView.inputField.text = @"";
    self.chatView.frame = CGRectMake(self.chatView.frame.origin.x, self.chatView.frame.origin.y - kbFrame.size.height, self.chatView.frame.size.width, self.chatView.frame.size.height);
    
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue]];
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.chatView.inputField.text = @" Message";
    CGRect kbFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.chatView.frame = CGRectMake(self.chatView.frame.origin.x, self.chatView.frame.origin.y + kbFrame.size.height, self.chatView.frame.size.width, self.chatView.frame.size.height);
    
    [UIView commitAnimations];
}


@end
