//
//  InTableViewCell.h
//  VKChat
//
//  Created by Valery Efimov on 05.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageDBModel+CoreDataClass.h"
#import <Masonry.h>

@interface InTableViewCell : UITableViewCell

- (id)initWithStyle: (UITableViewCellStyle)style
      reuseIndefier: (NSString *)indefier;

- (void)configureUIWithMessage:(MessageDBModel *) message;

@property (strong,nonatomic) UIImageView *avatar;
@property (strong,nonatomic) UILabel *time;
@property (strong,nonatomic) UILabel *messagesIndex;
@property (strong,nonatomic) UIView *dataView;
@property (strong,nonatomic) UILabel *text;

@end
