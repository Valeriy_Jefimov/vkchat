//
//  InTableViewCell.m
//  VKChat
//
//  Created by Valery Efimov on 05.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "InTableViewCell.h"
#import "UIImageView+Downloader.h"
#import "BaseTableViewCell.h"

@interface InTableViewCell()

@property(strong,nonatomic) UIColor *dataViewColor;
@property(assign,nonatomic) CGFloat dataViewWidth;

@end


@implementation InTableViewCell


- (id)initWithStyle: (UITableViewCellStyle)style
      reuseIndefier: (NSString *)indefier {
    self = [super initWithStyle:style reuseIdentifier:indefier];
    
     self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.dataViewColor = [UIColor colorWithRed:0.90 green:0.91 blue:0.93 alpha:1.0];
    self.backgroundColor = [UIColor clearColor];
    [self setupDataView];
    [self setupTextLabel];
    //[self setupTimeLabel];
    [self setupImageAvatar];
    // [self setupMessagesIndexLabel];

    return self;
}


#pragma mark - ConfigureUI


- (void)configureUIWithMessage:(MessageDBModel *)message {
    [self configureDataViewWithMessage:message];
    [self configureImageWithChat:message];
    [self configureTextLabelWithChat:message];
    //        [self configureTimeLabelWithChat:message];
    //        [self configureMessagesIndexLabelWithChat:message];
}

- (void)configureDataViewWithMessage:(MessageDBModel *)message {
    float bodySizeWidth = message.bodySizeWidth;
    float bodySizeHeight = message.bodySizeHeight;

    self.dataView.frame = CGRectMake(50, 7, bodySizeWidth + 14, bodySizeHeight + 14);
}

- (void)configureTimeLabelWithChat:(MessageDBModel *)message {
    self.time.text = NSLocalizedString(message.date,nil);
    self.time.alpha = 1;
}

- (void)configureTextLabelWithChat:(MessageDBModel *)message {
    self.text.frame = CGRectMake(7, 7, message.bodySizeWidth, message.bodySizeHeight );
    self.text.text = message.body;
}

- (void)configureMessagesIndexLabelWithChat:(MessageDBModel *)message {
    
}

- (void)configureImageWithChat:(MessageDBModel*)message {
    
//    if(message.image || message.chatID == 0)
//    {
//            self.avatar.image = message.image;
//            self.avatar.alpha = 1.0;
//    }
}



#pragma mark - SetupUI


- (void)setupDataView {
    self.dataView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:self.dataView];
    self.dataView.layer.cornerRadius = 10;
    self.dataView.layer.masksToBounds = NO;
    self.dataView.backgroundColor = self.dataViewColor;
    self.contentView.backgroundColor = [UIColor clearColor];;
}

- (void) setupTextLabel {
    self.text = [UILabel new];
    [self.dataView addSubview: self.text];
    self.text.font = [UIFont fontWithName:@"Avenir-Roman" size:12];
    self.text.numberOfLines = 0;

}

- (void)setupImageAvatar {
    self.avatar = [[UIImageView alloc] init];
    [self.contentView addSubview:self.avatar];
    
    __weak typeof (self) wSelf = self;
    [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wSelf).offset(5);
        make.left.equalTo(wSelf).offset(5);
        make.height.equalTo(@(30));
        make.width.equalTo(@(30));
    }];
    self.avatar.layer.cornerRadius = 15;
    self.avatar.layer.masksToBounds = YES;
    self.avatar.backgroundColor = [UIColor grayColor];
    self.avatar.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)setupTimeLabel {
    __weak typeof (self) wSelf = self;
    self.time = [UILabel new];
    [self.contentView addSubview:self.time];
    [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wSelf).offset(10);
        make.right.equalTo(wSelf).offset(-15);
    }];
    self.time.font = [UIFont systemFontOfSize:12.0];
    self.time.textColor = [UIColor grayColor];
    [self.time setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
}

- (void)setupMessagesIndexLabel {
    self.messagesIndex = [UILabel new];
    [self.contentView addSubview:self.messagesIndex];
    self.messagesIndex.backgroundColor = [UIColor clearColor];
    self.messagesIndex.font = [UIFont systemFontOfSize:12.0];
    self.messagesIndex.textAlignment = NSTextAlignmentCenter;
    self.messagesIndex.textColor = [UIColor grayColor];
    self.messagesIndex.hidden = YES;
}

- (void)setSelected: (BOOL) selected animated:(BOOL)animated {
    [super setSelected:selected animated:NO];

}

@end
