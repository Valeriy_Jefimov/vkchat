//
//  OutTableViewCell.m
//  VKChat
//
//  Created by Valery Efimov on 09.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "OutTableViewCell.h"
#import <Masonry.h>
#import "BaseTableViewCell.h"

@interface OutTableViewCell()

@property(strong,nonatomic) UIColor *dataViewColor;
@property(assign,nonatomic) CGFloat dataViewWidth;

@end



@implementation OutTableViewCell


- (id)initWithStyle: (UITableViewCellStyle)style
      reuseIndefier: (NSString *)indefier {
    self = [super initWithStyle:style reuseIdentifier:indefier];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
     self.dataViewColor = [UIColor colorWithRed:0.28 green:0.51 blue:0.69 alpha:1.0];
     self.backgroundColor = [UIColor clearColor];
     [self setupDataView];
     [self setupTextLabel];
    
    //[self setupTimeLabel];
    //[self setupMessagesIndexLabel];
    
    [self.contentView updateConstraints];
    return self;
}


#pragma mark - ConfigureUI


- (void)configureUIWithMessage:(MessageDBModel *)message {
    [self configureDataViewWithMessage:message];
    [self configureTextLabelWithChat:message];
    //[self configureTimeLabelWithChat:message];
    //[self configureMessagesIndexLabelWithChat:message];
}

- (void)configureDataViewWithMessage:(MessageDBModel *)message {
    CGFloat x = self.contentView.frame.size.width - message.bodySizeWidth - 21;
    self.dataView.frame = CGRectMake(x, 7, message.bodySizeWidth + 14, message.bodySizeHeight + 14);
}

- (void)configureTimeLabelWithChat:(MessageDBModel *)message {
    self.time.text = NSLocalizedString(message.date,nil);
    self.time.alpha = 1;
}

- (void)configureMessagesIndexLabelWithChat:(MessageDBModel *)message {
    
}

- (void)configureTextLabelWithChat:(MessageDBModel *)message {
    self.text.frame = CGRectMake(7, 7, message.bodySizeWidth, message.bodySizeHeight);
    self.text.text = message.body;
}


#pragma mark - UISetup


- (void)setupDataView {
    self.dataView = [[UIView alloc] init];
    [self.contentView addSubview:self.dataView];
    self.dataView.layer.cornerRadius = 10;
    self.dataView.layer.masksToBounds = NO;
    self.dataView.backgroundColor = self.dataViewColor;
    self.contentView.backgroundColor = [UIColor clearColor];;
}

- (void) setupTextLabel {
    self.text = [UILabel new];
    [self.dataView addSubview: self.text];
    self.text.textColor = [UIColor whiteColor];
    //self.text.textAlignment = NSTextAlignmentRight;
    self.text.font = [UIFont fontWithName:@"Avenir-Roman" size:12];
    self.text.numberOfLines = 0;

}

//- (void)updateConstraints {
//    [self.dataView mas_updateConstraints:^(MASConstraintMaker *make) {
//        CGFloat width = [BaseTableViewCell getSizeForText:self.text.text maxWidth:self.dataViewWidth font:@"Avenir-Roman" fontSize:12].width + 20;
//        make.width.equalTo(@(width));
//        make.right.equalTo(self.contentView.mas_right).offset(-7);
//        make.top.equalTo(self.contentView.mas_top).offset(5);
//        make.bottom.equalTo(self.contentView.mas_bottom).offset(-5);
//    }];
//    
//  
//    [self.text mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.dataView.mas_top).offset(7);
//        make.left.equalTo(self.dataView.mas_left).offset(7);
//        make.bottom.equalTo(self.dataView.mas_bottom).offset(- 7);
//        make.right.equalTo(self.dataView.mas_right).offset(- 7);
//    }];
//    [super updateConstraints];
//}


@end
