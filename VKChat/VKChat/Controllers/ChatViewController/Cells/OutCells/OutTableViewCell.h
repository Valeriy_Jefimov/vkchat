//
//  OutTableViewCell.h
//  VKChat
//
//  Created by Valery Efimov on 09.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageDBModel+CoreDataProperties.h"


@interface OutTableViewCell : UITableViewCell

- (id)initWithStyle: (UITableViewCellStyle)style
      reuseIndefier: (NSString *)indefier;

- (void)configureUIWithMessage:(MessageDBModel *)message;


@property (strong,nonatomic) UILabel *time;
@property (strong,nonatomic) UILabel *messagesIndex;
@property (strong,nonatomic) UIView *dataView;
@property (strong,nonatomic) UILabel *text;


@end
