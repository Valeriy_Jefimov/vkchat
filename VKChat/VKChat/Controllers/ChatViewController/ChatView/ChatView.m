//
//  ChatView.m
//  VKChat
//
//  Created by Valery Efimov on 04.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "ChatView.h"
#import <Masonry.h>
#import "SVPullToRefresh.h"

static NSInteger  const kButtonSize =             40;
static NSInteger  const kOffset =                  7;

@interface ChatView ()

@property(strong,nonatomic) UIImageView *backGroundView;
@property (strong,nonatomic) id<ChatProtocol> delegate;
@property(strong,nonatomic) UIView *textInputView;
@property(strong,nonatomic) UIColor *color;


@end


@implementation ChatView

#pragma mark - LifeCycle


- (instancetype)initWithFrame:(CGRect)frame  {
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.color = [UIColor colorWithRed:0.22 green:0.41 blue:0.58 alpha:1.0];
        self.userInteractionEnabled = YES;
        [self setupView];
    }
    return self;
}

-(void)setTableDelegat:(id)aDelegate
                      chatDelegate:(id<ChatProtocol>)aChatDelegate
                    chatDataSource:(id<UITableViewDataSource>)aDataSource
            inputTextFieldDelegate:(id<UITextViewDelegate>)aTextFieldDelegate
   attacmentCollectionViewDelegate:(id<UICollectionViewDelegate>)attachmentCollectionViewDelegate
 attacmentCollectionViewDataSource:(id<UICollectionViewDataSource>)attachmentCollectionViewDataSource {
    self.tableView.delegate = aDelegate;
    self.tableView.dataSource = aDataSource;
    self.delegate = aChatDelegate;
    self.inputField.delegate = aTextFieldDelegate;
    self.attachMentsCollectionView.delegate = attachmentCollectionViewDelegate;
    self.attachMentsCollectionView.dataSource = attachmentCollectionViewDataSource;
}


#pragma mark - UI


- (void)setupView {
    [self addBackGroundViewToView];
    [self addTableToView];
    [self addTextInputViewToView];
    [self setupTextInputUI];
}

- (void)addBackGroundViewToView {
    self.backGroundView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.backGroundView.backgroundColor = [UIColor clearColor];
    self.backGroundView.userInteractionEnabled = YES;
    self.backGroundView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.backGroundView];
}

- (void)addTableToView {
    self.tableView = [[UITableView alloc]initWithFrame:self.frame];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self.backGroundView addSubview:self.tableView];
    __weak typeof (self) wSelf = self;
    
    [self.tableView addPullToRefreshWithActionHandler:^{
        wSelf.tableView.pullToRefreshView.alpha = 1.0;
        [wSelf.delegate loadMoreChatData];
    } position:SVPullToRefreshPositionTop];
    [self setupPullToRefreshView];
}

- (void)setupPullToRefreshView {

    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.frame = CGRectMake(0, 0, 24, 24);
    spinner.color = self.color;
    [spinner startAnimating];
    
    [self.tableView.pullToRefreshView setCustomView:spinner forState:SVPullToRefreshStateAll];
}

- (void)addTextInputViewToView {
    self.textInputView = [UIView new];
    self.textInputView.backgroundColor = self.color;
    self.textInputView.userInteractionEnabled = YES;
    [self.backGroundView addSubview:self.textInputView];
}


#pragma mark - TextInputView


-(void)setupTextInputUI {
        self.attachmentButton = [UIButton new];
        [self.attachmentButton addTarget:self
                                  action:@selector(attachmentButtonPressed:)
                        forControlEvents:UIControlEventTouchUpInside];
        [self.attachmentButton setImage:[UIImage imageNamed:@"attach"] forState:UIControlStateNormal];
        [self.textInputView addSubview:self.attachmentButton];
        
        self.sendButton = [UIButton new];
        [self.sendButton addTarget:self
                            action:@selector(sendButtonPressed:)
                  forControlEvents:UIControlEventTouchUpInside];
        [self.sendButton setImage:[UIImage imageNamed:@"send"] forState:UIControlStateNormal];
        [self.textInputView addSubview:self.sendButton];
        
        self.inputView = [UIView new];
        self.inputView.backgroundColor = [UIColor whiteColor];
        self.inputView.clipsToBounds = YES;
        self.inputView.layer.cornerRadius = 10;
        self.inputView.userInteractionEnabled = YES;
        [self.textInputView addSubview:self.inputView];
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        [self.attachMentsCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
        self.attachMentsCollectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        self.attachMentsCollectionView.backgroundColor = [UIColor redColor];
        [self.inputView addSubview: self.attachMentsCollectionView];
        
        self.inputField = [UITextView new];
        self.inputField.text = @" Message";
        self.inputField.textColor = [UIColor grayColor];
        self.inputField.textAlignment = NSTextAlignmentLeft;
        self.inputField.font = [UIFont systemFontOfSize:16];
        self.inputField.backgroundColor = [UIColor clearColor];
        self.inputField.editable = YES;
        self.inputField.userInteractionEnabled = YES;
        [self.inputView addSubview:self.inputField];
}


#pragma mark - Constraints


- (void)setupBackGroundViewConstraints {
    __weak typeof (self) wSelf = self;
    [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wSelf);
        make.bottom.equalTo(wSelf);
        make.left.equalTo(wSelf);
        make.right.equalTo(wSelf);
    }];
}

- (void)setupTableViewConstraints {
    __weak typeof (self) wSelf = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wSelf.backGroundView.mas_top);
        make.bottom.equalTo(wSelf.textInputView.mas_top);
        make.left.equalTo(wSelf.backGroundView.mas_left);
        make.right.equalTo(wSelf.backGroundView.mas_right);
    }];
    
}


- (void)setupTextInputViewConstraints {
    __weak typeof (self) wSelf = self;
    [self.inputField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(wSelf.inputView.mas_bottom);
        make.right.equalTo(wSelf.inputView.mas_right);
        make.left.equalTo(wSelf.inputView.mas_left);
        make.top.equalTo(wSelf.attachMentsCollectionView.mas_bottom);
        make.height.equalTo(@(wSelf.frame.size.height / 15));
    }];
    
    [self.attachMentsCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(0));
        make.right.equalTo(wSelf.inputView.mas_right);
        make.left.equalTo(wSelf.inputView.mas_left);
        make.top.equalTo(wSelf.inputView.mas_top);
    }];
    
    [self.textInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wSelf.tableView.mas_bottom).priorityLow(@(800));
        make.bottom.equalTo(wSelf.backGroundView.mas_bottom);
        make.right.equalTo(wSelf.backGroundView.mas_right);
        make.left.equalTo(wSelf.backGroundView.mas_left);
    }];
    
    [self.attachmentButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(wSelf.textInputView.mas_bottom);
        make.height.equalTo(@(kButtonSize));
        make.width.equalTo(@(kButtonSize));
        make.left.equalTo(wSelf.textInputView.mas_left);
    }];
    
    [self.sendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(wSelf.textInputView.mas_bottom);
        make.right.equalTo(wSelf.textInputView.mas_right);
        make.height.equalTo(@(kButtonSize));
        make.width.equalTo(@(kButtonSize));
    }];
    
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(wSelf.textInputView.mas_bottom).offset(-kOffset);
        make.right.equalTo(wSelf.sendButton.mas_left);
        make.left.equalTo(wSelf.attachmentButton.mas_right);
        make.top.equalTo(wSelf.textInputView.mas_top).offset(kOffset);
    }];
 
}


- (void)updateConstraints {
    [super updateConstraints];
    
    [self setupBackGroundViewConstraints];
    [self setupTextInputViewConstraints];
    [self setupTableViewConstraints];
}


#pragma mark - Actions


- (void)attachmentButtonPressed:(id)sender {
    NSLog(@"-------------------------------attach");
    CGPoint bottomOffset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height);
    [self.tableView setContentOffset:bottomOffset animated:YES];
}

- (void)sendButtonPressed:(id)sender {
    NSLog(@"-------------------------------send");
    [self.inputField resignFirstResponder];
    CGPoint bottomOffset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height);
    [self.tableView setContentOffset:bottomOffset animated:YES];
}

- (void)scrollToBottom {
    if(self.tableView.contentSize.height < self.tableView.frame.size.height)
    {
        self.tableView.pullToRefreshView.alpha = 0.0;
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
        [self.tableView setContentOffset:offset animated:NO];
    }
    else
    {
        NSInteger numberOfRows = [self.tableView numberOfRowsInSection:0];
        if (numberOfRows)
        {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
    }
}

@end
