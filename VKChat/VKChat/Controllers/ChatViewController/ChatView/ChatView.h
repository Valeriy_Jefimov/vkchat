//
//  ChatView.h
//  VKChat
//
//  Created by Valery Efimov on 04.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChatProtocol <NSObject>

- (void)loadMoreChatData;

@end

@interface ChatView : UIView


-(void)setTableDelegat:(id)aDelegate
                      chatDelegate:(id<ChatProtocol>)aChatDelegate
                    chatDataSource:(id<UITableViewDataSource>)aDataSource
            inputTextFieldDelegate:(id<UITextViewDelegate>)aTextFieldDelegate
   attacmentCollectionViewDelegate:(id<UICollectionViewDelegate>)attachmentCollectionViewDelegate
 attacmentCollectionViewDataSource:(id<UICollectionViewDataSource>)attachmentCollectionViewDataSource;

- (void)scrollToBottom;

//textInput
@property(strong,nonatomic) UIButton *attachmentButton;
@property(strong,nonatomic) UIButton *sendButton;
@property(strong,nonatomic) UITextView *inputField;
@property(strong,nonatomic) UIView *inputView;
@property(strong,nonatomic) UICollectionView *attachMentsCollectionView;
@property(strong,nonatomic) UITableView *tableView;

@end
