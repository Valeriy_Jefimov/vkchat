//
//  ChatViewController.h
//  VKChat
//
//  Created by Valery Efimov on 04.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatDataSource.h"
#import "ChatDBModel+CoreDataProperties.h"
#import "UIImageView+Downloader.h"
#import "AppSessionManager.h"

@interface ChatViewController : UIViewController<UITableViewDelegate,UISearchBarDelegate,TableDelegateProtocol>

@property(strong,nonatomic) NSMutableArray *data;
@property(strong,nonatomic) ChatDBModel *model;
@property(strong,nonatomic) ChatDataSource *dataSourse;

- (instancetype)initWithChatModel:(ChatDBModel *)model sessionManager:(AppSessionManager *)sessionManager;
- (void)setupUserImage:(UIImage*)userImage;

@end
