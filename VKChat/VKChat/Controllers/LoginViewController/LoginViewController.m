//
//  LoginViewController.m
//  VKTest
//
//  Created by Valery Efimov on 01.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import "LoginViewController.h"
#import "AppSessionManager.h"
#import "LoginView.h"
#import "LongPollsActions.h"
#import "ChatListViewController.h"


@interface LoginViewController ()<NSFetchedResultsControllerDelegate>

@property(strong,nonatomic) LoginView *loginView;
@property(strong,nonatomic) NSMutableArray *data;
@property(strong,nonatomic) AppSessionManager *sessionManager;

@end

@implementation LoginViewController


#pragma mark - LifeCycle


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self notificationSetup];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        self.sessionManager = [AppSessionManager new];
        [self.sessionManager.vkManager setUIDelegate:self];
        [self.sessionManager chekReachable];
        [self.loginView stopActivityIndicator];
    });
   
}
- (void)notificationSetup {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkFailed)
                                                 name:@"networkFailed"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupChatsViewController)
                                                 name:@"authFinished"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(login)
                                                 name:@"authNeeded"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkGood)
                                                 name:@"networkGood"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupChatsViewController)
                                                 name:@"authUnNeed"
                                               object:nil];
}

- (void)loadView {
    [super loadView];
    
    self.loginView = [LoginView new];
    self.view = self.loginView;
    [self.loginView setNeedsUpdateConstraints];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.navigationController.navigationBar setHidden:YES];
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupChatsViewController {
    [self.loginView startActivityIndicator];
    ChatListViewController *chatsViewController = [[ChatListViewController alloc]initWithSessionManager:self.sessionManager];
    [self.sessionManager setupDataForChatsViewControllerWithCompletion:^(NSInteger dataCount, NSArray *data) {
        [self moveToChats:chatsViewController];
    }];
}


- (void)moveToChats:(ChatListViewController *) chats {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:chats animated:YES];
        [self.navigationController.navigationBar setHidden:NO];
    });
}


#pragma mark - Notification Actions


- (void)networkFailed {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:NSLocalizedString(@"Network fail, try later",nil)
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self.loginView needsUpdateConstraints];
                                       [self.loginView stopActivityIndicator];
                                       [self setupChatsViewController];
                                   }];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)login {
    [self.sessionManager.vkManager autorize];
}

- (void)networkGood {
    [self.sessionManager.vkManager login];
}


#pragma mark - VKSdkUIDelegate


-(void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    
    [self.navigationController presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkWillDismissViewController:(UIViewController *)controller {
    
}

- (void)vkSdkDidDismissViewController:(UIViewController *)controller {
    
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self];
}


#pragma mark - Other 


@end
