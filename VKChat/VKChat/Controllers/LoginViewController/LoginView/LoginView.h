//
//  LoginView.h
//  VKChat
//
//  Created by Valery Efimov on 13.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginView : UIView


- (void) startActivityIndicator;
- (void) stopActivityIndicator;
- (void) removeActivityIndicatorFromSuperview;


@end
