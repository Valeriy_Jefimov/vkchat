//
//  LoginView.m
//  VKChat
//
//  Created by Valery Efimov on 13.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import "LoginView.h"
#import <Masonry.h>
#import <DGActivityIndicatorView.h>

@interface LoginView ()

@property (strong, nonatomic)  UIImageView *backGroundView;
@property (strong, nonatomic)  DGActivityIndicatorView *activityIndicatorView;
@property (strong, nonatomic)  UIButton *logButton;

@end


@implementation LoginView


#pragma mark - LifeCycle


- (instancetype)initWithFrame:(CGRect)frame  {
    self = [super initWithFrame:frame];
    if (self)
    {
       [self setupView];
        
    }
    return self;
}


#pragma mark - UI


- (void)setupView {
    
    [self addBackGroundViewToView];
    [self addButtonToView];
    [self addActivityindicatorToView];
}

- (void)addBackGroundViewToView {
    
    self.backGroundView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.backGroundView.image = [UIImage imageNamed:@"bg6+"];
    [self addSubview:self.backGroundView];
}

- (void)addActivityindicatorToView {
    
    UIColor *color = [UIColor colorWithRed:0.2784 green: 0.513 blue:0.6941 alpha:1.0];
    self.activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallRotate tintColor:color size:100.0f];
    self.activityIndicatorView.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    [self.backGroundView addSubview:self.activityIndicatorView];
}

- (void)addButtonToView {
    
    UIButton *log = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    [log setBackgroundImage:[UIImage imageNamed:@"button"] forState:UIControlStateNormal];
    self.logButton = log;
    [self.logButton addTarget:self action:@selector(logPressed:)forControlEvents:UIControlEventTouchUpInside];
    [self addSubview: self.logButton];
}


#pragma mark - Constraints


- (void)setupBackGroundViewConstraints {
    __weak typeof (self) wSelf = self;
    [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wSelf);
        make.bottom.equalTo(wSelf);
        make.left.equalTo(wSelf);
        make.right.equalTo(wSelf);
    }];
}

- (void)setupLogButtonConstraints {
    __weak typeof (self) wSelf = self;
    [self.logButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wSelf).offset(wSelf.frame.size.height / 2 );
        make.bottom.equalTo(wSelf).offset(-(wSelf.frame.size.height / 8 * 3 ));
        make.left.equalTo(wSelf).offset(wSelf.frame.size.width / 5);
        make.right.equalTo(wSelf).offset(-(wSelf.frame.size.width / 5));
    }];
}


- (void)setupActivityIndicatorViewConstraints {
    __weak typeof (self) wSelf = self;
    [self.activityIndicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(wSelf);
        make.top.equalTo(wSelf).offset(wSelf.frame.size.height / 2 );
    }];
}

- (void)updateConstraints {
    [super updateConstraints];
    
    [self setupBackGroundViewConstraints];
    [self setupActivityIndicatorViewConstraints];
    [self setupLogButtonConstraints];
    [self startActivityIndicator];
}


#pragma mark - ActivityIndicator


- (void)startActivityIndicator {
    [self.activityIndicatorView startAnimating];
    [self.activityIndicatorView setHidden:NO];
    [self.logButton setHidden:YES];
}

- (void)stopActivityIndicator {
    [self.activityIndicatorView stopAnimating];
    [self.activityIndicatorView setHidden:YES];
    [self.logButton setHidden:NO];

}

- (void)removeActivityIndicatorFromSuperview {
    [self.activityIndicatorView removeFromSuperview];
}


#pragma mark - LogButton


- (void)logPressed:(id)sender {
    [self startActivityIndicator];
    self.activityIndicatorView.alpha = 1;
    [[NSNotificationCenter defaultCenter]postNotificationName:@"authNeeded" object:nil];
}

@end
