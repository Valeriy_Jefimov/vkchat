//
//  LoginViewController.h
//  VKTest
//
//  Created by Valery Efimov on 01.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <VKSdk.h>

@interface LoginViewController : UIViewController<UIWebViewDelegate,VKSdkUIDelegate>

@end
