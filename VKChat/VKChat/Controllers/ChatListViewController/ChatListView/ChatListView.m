//
//  ChatListView.m
//  VKChat
//
//  Created by Valery Efimov on 30.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "ChatListView.h"
#import "SVPullToRefresh.h"



@interface ChatListView ()

@property (strong,nonatomic) id<ChatListProtocol> delegate;
@property(strong,nonatomic) UIColor *color;


@end
@implementation ChatListView

- (instancetype)initWithFrame:(CGRect)frame  {
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.color = [UIColor colorWithRed:0.22 green:0.41 blue:0.58 alpha:1.0];
        self.userInteractionEnabled = YES;
        [self setupView];
    }
    return self;
}

-(void)setTableDelegat:(id)aDelegate
          chatDelegate:(id<ChatListProtocol>)aChatDelegate
    chatListDataSource:(id<UITableViewDataSource>)aDataSource
    seacrchBarDelegate:(id<ILRemoteSearchBarDelegate>) searchBarADelegate {
    self.tableView.delegate = aDelegate;
    self.tableView.dataSource = aDataSource;
    self.delegate = aChatDelegate;
    self.searchBar.delegate = searchBarADelegate;
    
    __weak typeof (self) wSelf = self;
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [wSelf.delegate loadMoreChatData];
    }];
}


#pragma mark - UI


- (void)setupView {
    [self tableViewSetup];
    [self searchBarSetup];
}

- (void)tableViewSetup {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView = [[UITableView alloc]initWithFrame:self.frame];
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.tableView];
}

- (void)searchBarSetup {
    self.searchBar = [[ILRemoteSearchBar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50)];
    self.searchBar.placeholder = @"Search";
    self.searchBar.timeToWait = 0.01;
    self.searchBar.barTintColor = [UIColor colorWithRed:0.90 green:0.91 blue:0.94 alpha:1.0];
    [self.tableView setTableHeaderView:self.searchBar];
    self.searchBar.backgroundColor = [UIColor colorWithRed:0.90 green:0.91 blue:0.94 alpha:1.0];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setBackgroundColor:[UIColor whiteColor]];
}


#pragma mark - Constraints



//- (void)setupTableViewConstraints {
//    __weak typeof (self) wSelf = self;
//    [wSelf.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(wSelf.mas_top);
//        make.bottom.equalTo(wSelf.mas_bottom);
//        make.left.equalTo(wSelf.backGroundView.mas_left);
//        make.right.equalTo(wSelf.backGroundView.mas_right);
//    }];
//    [wSelf.tableView mas]
//    
//}


- (void)updateConstraints {
    [super updateConstraints];

   // [self setupTableViewConstraints];
}


#pragma mark - Actions



- (void)scrollToBottom {
    if(self.tableView.contentSize.height < self.tableView.frame.size.height)
    {
        self.tableView.pullToRefreshView.alpha = 0.0;
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
        [self.tableView setContentOffset:offset animated:NO];
    }
    else
    {
        NSInteger numberOfRows = [self.tableView numberOfRowsInSection:0];
        if (numberOfRows)
        {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
    }
}


@end
