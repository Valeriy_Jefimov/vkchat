//
//  ChatListView.h
//  VKChat
//
//  Created by Valery Efimov on 30.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILRemoteSearchBar.h"

@protocol ChatListProtocol <NSObject>

- (void)loadMoreChatData;

@end

@interface ChatListView : UIView

-(void)setTableDelegat:(id)aDelegate
          chatDelegate:(id<ChatListProtocol>)aChatDelegate
    chatListDataSource:(id<UITableViewDataSource>)aDataSource
    seacrchBarDelegate:(id<ILRemoteSearchBarDelegate>) searchBarADelegate;

- (void)scrollToBottom;

@property(strong,nonatomic) UITableView *tableView;
@property(strong,nonatomic) ILRemoteSearchBar *searchBar;

@end
