//
//  ChatListViewController.h
//  VKChat
//
//  Created by Valery Efimov on 15.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILRemoteSearchBar.h"
#import "ChatsDataSource.h"
#import "AppSessionManager.h"
#import "JHChainableAnimations.h"
#import "ChatsTableViewCell.h"
#import "LongPollsActions.h"
#import "ChatListView.h"



@interface ChatListViewController : UIViewController<UITableViewDelegate,UISearchBarDelegate,ILRemoteSearchBarDelegate,TableDelegateProtocol,ChatListProtocol,ChatListControllerProtocol>

@property(strong,atomic) ChatsDataSource *dataSource;


- (instancetype)initWithSessionManager:(AppSessionManager *)sessionManager;

@end
