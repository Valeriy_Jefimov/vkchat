//
//  contactsDataSourse.h
//  Contacts
//
//  Created by Valery Efimov on 03.11.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseTableViewDataSource.h"
#import "ChatsTableViewCell.h"
#import "SearchChatsTableViewCell.h"
#import "SearchChatsModel.h"

@interface ChatsDataSource : BaseTableViewDataSource <NSFetchedResultsControllerDelegate>
@property (strong,nonatomic) NSMutableArray *searchData;
@property (assign,nonatomic) BOOL isSearch;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;


- (void)configureSearchDataWithArray:(NSArray*)searchData;
- (instancetype)initWithSessionmanager:(AppSessionManager*)sessionManager;
- (void)setTableDelegate:(id<TableDelegateProtocol>)aDelegate;

@end
