//
//  contactsDataSourse.m
//  Contacts
//
//  Created by Valery Efimov on 03.11.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import "ChatsDataSource.h"



@interface ChatsDataSource ()

@property(strong,nonatomic) id<TableDelegateProtocol> delegate;
@property(strong,nonatomic) NSCache *cashe;

@end

@implementation ChatsDataSource


#pragma mark - LifeCycle


- (instancetype)initWithSessionmanager:(AppSessionManager*)sessionManager   {
    self = [super init];
    if (self)
    {
        self.searchData  = [NSMutableArray new];
        self.sessionManager = sessionManager;
        [NSFetchedResultsController deleteCacheWithName:@"Root"];
        [self setupFetchedResultsController];
        self.cashe = [NSCache new];
        self.isSearch = NO;
        
    }
    return self;
}

- (void)setTableDelegate:(id<TableDelegateProtocol>)aDelegate {
    self.delegate = aDelegate;
}

#pragma mark - Fetched Results Controller


- (void)setupFetchedResultsController {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"ChatDBModel" inManagedObjectContext:[self.sessionManager parentContext]];
    [fetchRequest setEntity:entity];
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"creationDate" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    [fetchRequest setFetchBatchSize:30];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:[self.sessionManager parentContext] sectionNameKeyPath:nil
                                                   cacheName:@"Root"];
    self.fetchedResultsController = theFetchedResultsController;
    self.fetchedResultsController.delegate = self;
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }

}

#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isSearch)
    {
        return self.searchData.count;
    }
    return self.fetchedResultsController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatsTableViewCell *cell = (ChatsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ChatsTableViewCell class])];
    if (!self.isSearch)
    {
        if (cell == nil)
        {
            cell = [[ChatsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault  reuseIndefier:NSStringFromClass([ChatsTableViewCell class]) ];
        }
        cell.messagesIndex.hidden = YES;
        [cell setupUIWithChat:[self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.row]];
    }
    else
    {
        SearchChatsTableViewCell *searchCell = (SearchChatsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchChatsTableViewCell class])];
        if (searchCell == nil)
        {
            searchCell = [[SearchChatsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault  reuseIndefier:NSStringFromClass([SearchChatsTableViewCell class]) ];
        }
        [searchCell configureCellWithChat:self.searchData [indexPath.row]];
        
        return searchCell;
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.isSearch)
    {
        return NO;
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView
 commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
  forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        ChatDBModel *objectToDelete = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [[self.sessionManager parentContext]  deleteObject:objectToDelete];
        if(!objectToDelete.chatID)
        {
            [self.sessionManager.vkManager messagesDeleteDialogWithUser:objectToDelete.userID peerId:@"" completion:^{
                NSLog(@"deleted");
            }];
        }
        else
        {
            NSInteger i = [objectToDelete.chatID integerValue] + kPeer;
            [self.sessionManager.vkManager messagesDeleteDialogWithUser:@"" peerId:[NSString stringWithFormat:@"%ld",(long)i] completion:^{
                NSLog(@"deleted");
            }];
        }
        [self.sessionManager saveContext: self.sessionManager.childContext];
    }
}


- (void)configureSearchDataWithArray:(NSArray*)searchData {
    for (id model in searchData)
    {
        SearchChatsModel *mode = [[SearchChatsModel alloc] initWithJson:model];
        [_searchData addObject:mode];
    }
    self.isSearch = YES;
}

- (NSInteger)dataCount {
    if (self.isSearch)
    {
        return self.searchData.count;
    }
    return self.fetchedResultsController.fetchedObjects.count;
}


#pragma mark - NSFetchedResultsControllerDelegate


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.delegate beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.delegate insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.delegate deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.delegate insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.delegate deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeUpdate: {
            ChatsTableViewCell *cell = (ChatsTableViewCell *)[self.delegate getCellForRowAtIndexPath:indexPath];
            [cell setupUIWithChat:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        }
            break;
        case NSFetchedResultsChangeMove:
            [self.delegate deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationNone];
            [self.delegate insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.delegate endUpdates];
}

@end
