//
//  ChatsTableViewCell.h
//  VKChat
//
//  Created by Valery Efimov on 26.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatDBModel+CoreDataClass.h"
#import "BaseTableViewCell.h"

@interface ChatsTableViewCell : BaseTableViewCell



- (void)setupUIWithChat:(ChatDBModel *)chat;

@property (strong,nonatomic) UIImageView *avatar;
@property (strong,nonatomic) UILabel *time;
@property (strong,nonatomic) UILabel *messagesIndex;
@property (strong,nonatomic) UILabel *message;
@property (strong,nonatomic) UIImageView *type;
@property (strong,nonatomic) UILabel *fullName;
@property (strong,nonatomic) UIView *bgMessage;
@property (strong,nonatomic) UIView *cardView;

@end
