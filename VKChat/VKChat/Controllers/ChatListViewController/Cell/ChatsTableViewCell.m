//
//  ChatsTableViewCell.m
//  VKChat
//
//  Created by Valery Efimov on 26.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import "ChatsTableViewCell.h"

#import "UIImageView+Downloader.h"
#import <Masonry.h>
#import <DGActivityIndicatorView.h>
#import "ChatViewController.h"


@interface ChatsTableViewCell()


@end


@implementation ChatsTableViewCell
- (id)initWithStyle: (UITableViewCellStyle)style
      reuseIndefier: (NSString *)indefier {
    self = [super initWithStyle:style reuseIdentifier:indefier];
 
        [self setupCardView];
        [self setupTimeLabel];
        [self setupImageAvatar];
        [self setupMessageLabel];
        [self setupNameLabel];
        [self setupPlatformImage];
        [self setupNewMessagesIndexLabel];

    
    return self;
}


#pragma mark - SetupUI


- (void)setupUIWithChat:(ChatDBModel *)chat {
        [self configureImageWithChat:chat];
        [self configureTimeLabelWithChat:chat];
        [self configureMessageLabelWithChat:chat];
        [self configureNameLabelWithChat:chat];
        [self configurePlatformImageWithChat:chat];
        [self configureNewMessageLabelWithChat:chat];
}

- (void)configureTimeLabelWithChat:(ChatDBModel *)chat {
    self.time.text                          = NSLocalizedString(chat.time,nil);
    self.time.alpha                         = 1;
}

- (void)configureMessageLabelWithChat:(ChatDBModel *)chat {
    self.bgMessage.backgroundColor = [UIColor clearColor];
    self.cardView.backgroundColor = [UIColor whiteColor];
    
    if ([[NSString stringWithFormat:@"%@",chat.readState] isEqualToString:@"0"])
    {
        if ( [[NSString stringWithFormat:@"%@",chat.unread] isEqualToString:@"0"])
        {
            
            self.cardView.backgroundColor   = [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:0.3];
        }
        else
        {
            self.bgMessage.backgroundColor  = [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:0.3];
        }
    }
    else
    {
        self.cardView.backgroundColor       = [UIColor whiteColor];
        self.bgMessage.backgroundColor      = [UIColor whiteColor];
    }
    self.message.text = NSLocalizedString(chat.body,nil);
    
}

- (void)configurePlatformImageWithChat:(ChatDBModel *)chat {
    self.type.image = [UIImage imageNamed:[chat currentUserPlatformImage] ];
}

- (void)configureNameLabelWithChat:(ChatDBModel *)chat {
        self.fullName.text = NSLocalizedString(chat.title,nil);
}

- (void)configureNewMessageLabelWithChat:(ChatDBModel *)chat {
        NSString *str = chat.unreadedCount;
        if([[NSString stringWithFormat:@"%@",chat.unread]isEqualToString:@"0"])
        {
            if([str integerValue] > 0)
            {
                self.messagesIndex.text     = str;
                self.messagesIndex.hidden   = NO;
            }
        }
        else
        {
            self.messagesIndex.hidden       = YES;
        }
}


- (void)configureImageWithChat:(ChatDBModel *)chat {
    UIImage *image = [chat chatImage];
    self.avatar.image = image;
}


#pragma mark - SetupUI


- (void)setupCardView {
    UIView *cardView = [[UIView alloc] init];
    [self.contentView addSubview:cardView];
    
    __weak typeof (self) wSelf       = self;
    [cardView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(wSelf);
        make.left.equalTo(wSelf);
        make.top.equalTo(wSelf).offset(2);
        make.bottom.equalTo(wSelf).offset(-2);
    }];
    
    cardView.layer.cornerRadius      = 3;
    cardView.layer.masksToBounds     = NO;
    cardView.backgroundColor         = [UIColor whiteColor];
    cardView.layer.shadowColor       = [UIColor colorWithRed:1.03 green:1.03 blue:1.03 alpha:1.0].CGColor;
    cardView.layer.shadowOffset      = CGSizeZero;
    cardView.layer.shadowOpacity     = 0.8;
    self.cardView = cardView;
    self.contentView.backgroundColor = [UIColor colorWithRed:0.90 green:0.91 blue:0.94 alpha:1.0];;
}

- (void)setupImageAvatar {
    UIImageView *imageView = [[UIImageView alloc] init];
    self.avatar = imageView;
    [self.contentView addSubview:self.avatar];
    
    __weak typeof (self) wSelf      = self;
    [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(wSelf);
        make.left.equalTo(wSelf).offset(10);
        make.height.equalTo(@(50));
        make.width.equalTo(@(50));
    }];
    self.avatar.layer.cornerRadius  = 25;
    self.avatar.layer.masksToBounds = YES;
    self.avatar.backgroundColor     = [UIColor grayColor];
    self.avatar.contentMode         = UIViewContentModeScaleAspectFill;
    
}
- (void)setupPlatformImage {
    UIImageView *imageView          = [UIImageView new];
    self.type = imageView;
    [self.contentView addSubview:self.type];
    
    __weak typeof (self) wSelf      = self;
    [self.type mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(wSelf.fullName.mas_right).offset(5);
        make.height.equalTo(@(8));
        make.width.equalTo(@(8));
        make.centerY.equalTo(wSelf.fullName.mas_centerY);
    }];
    
    self.type.backgroundColor       = [UIColor clearColor];
    self.type.contentMode           = UIViewContentModeScaleAspectFill;
}

- (void)setupTimeLabel {
    __weak typeof (self) wSelf      = self;
    self.time                       = [UILabel new];
    [self.contentView addSubview:self.time];
    [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wSelf).offset(10);
        make.right.equalTo(wSelf).offset(-15);
    }];
    self.time.font                  = [UIFont systemFontOfSize:12.0];
    self.time.textColor             = [UIColor grayColor];
    [self.time setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    
}

- (void)setupNewMessagesIndexLabel {
    __weak typeof (self) wSelf      = self;
    self.messagesIndex              = [UILabel new];
    [self.contentView addSubview:self.messagesIndex];
    [self.messagesIndex mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(wSelf.avatar.mas_bottom);
        make.right.equalTo(wSelf.avatar.mas_right);
        make.height.equalTo(@(20));
        make.width.equalTo(@(20));
    }];
    self.messagesIndex.backgroundColor      = [UIColor colorWithRed:0.87 green:0.44 blue:0.44 alpha:1.0];
    self.messagesIndex.font                 = [UIFont systemFontOfSize:12.0];
    self.messagesIndex.textAlignment        = NSTextAlignmentCenter;
    self.messagesIndex.textColor            = [UIColor whiteColor];
    self.messagesIndex.layer.cornerRadius   = 10;
    self.messagesIndex.layer.masksToBounds  = YES;
    self.messagesIndex.hidden               = YES;
}

- (void)setupMessageLabel {
    __weak typeof (self) wSelf              = self;
    UIView *bckgr                           = [UIView new];
    [self.contentView addSubview:bckgr];
    self.message                            = [UILabel new];
    [bckgr addSubview:self.message];
    [bckgr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(wSelf).offset(-20);
        make.height.equalTo(@(30));
        make.left.equalTo(wSelf.avatar.mas_right).offset(5);
        make.right.equalTo(wSelf).offset(-20);
    }];
    bckgr.backgroundColor                   = [UIColor clearColor];
    bckgr. alpha                            = 1;
    self.bgMessage                          = bckgr;
    [self.message mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(wSelf).offset(-20);
        make.height.equalTo(@(30));
        make.left.equalTo(wSelf.avatar.mas_right).offset(10);
        make.right.equalTo(wSelf).offset(-22);
    }];
    self.bgMessage.layer.cornerRadius       = 3;
    self.bgMessage.layer.masksToBounds      = YES;
    self.message.font                       = [UIFont systemFontOfSize:16.0];
    self.message.textColor                  = [UIColor grayColor];
    [self.message setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    
}
- (void)setupNameLabel {
    __weak typeof (self) wSelf              = self;
    self.fullName                           = [UILabel new];
    [self.contentView addSubview:self.fullName];
    [self.fullName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(15));
        make.left.equalTo(wSelf.avatar.mas_right).offset(10);
        make.bottom.equalTo(wSelf.message.mas_top).offset(-5);
        make.width.lessThanOrEqualTo(@(wSelf.frame.size.width / 2));
        
    }];
    self.fullName.font                      = [UIFont systemFontOfSize:16.0];
    self.fullName.textColor                 = [UIColor blackColor];
    [self.fullName sizeToFit];
}


- (void)setSelected: (BOOL) selected animated:(BOOL)animated {
    [super setSelected:selected animated:NO];
}

@end
