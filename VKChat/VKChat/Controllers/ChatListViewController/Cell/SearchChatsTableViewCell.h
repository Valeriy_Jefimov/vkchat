//
//  searchChatsTableViewCell.h
//  VKChat
//
//  Created by Valery Efimov on 02.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchChatsModel.h"
#import "BaseTableViewCell.h"

@interface SearchChatsTableViewCell : BaseTableViewCell

@property (strong,nonatomic) UIImageView *image;
@property (strong,nonatomic) UILabel *fullName;
@property (strong,nonatomic) UIView *cardView;


- (void)configureCellWithChat:(SearchChatsModel *)chat;


@end
