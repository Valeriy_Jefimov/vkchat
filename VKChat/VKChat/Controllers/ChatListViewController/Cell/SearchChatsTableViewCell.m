//
//  searchChatsTableViewCell.m
//  VKChat
//
//  Created by Valery Efimov on 02.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "SearchChatsTableViewCell.h"
#import <Masonry.h>
#import "UIImageView+Downloader.h"


@implementation SearchChatsTableViewCell

- (id)initWithStyle: (UITableViewCellStyle) style
      reuseIndefier: (NSString *) indefier {
    self = [super initWithStyle:style reuseIdentifier:indefier];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setupCardView];
        [self setupImageAvatar];
        [self setupNameLabel];

    });
    
    return self;
}


#pragma mark - Configure UI


- (void)configureCellWithChat:(SearchChatsModel *)chat {
        [self configureImageWithChat:chat];
        [self configureNameLabelWithChat:chat];
}

- (void)configureNameLabelWithChat:(SearchChatsModel *)chat {
    self.fullName.text = chat.title;
}

- (void)configureImageWithChat:(SearchChatsModel*)chat {
    self.image.image = nil;
    if (!chat.avatarImage)
    {
        NSString *photo = chat.photo100;
        [UIImageView loadImageWithURL:[NSURL URLWithString:photo]
                       WithCompletion:^(UIImage *image) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               chat.avatarImage = image;
                               self.image.image = image;
                               
                           });
                       }];
    } else
    {
        self.image.image                        = chat.avatarImage;
    }
    
}


#pragma mark - Setup UI


- (void)setupCardView {
    UIView *cardView = [[UIView alloc] init];
    [self.contentView addSubview:cardView];
    
    __weak typeof (self) wSelf = self;
    [cardView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(wSelf);
        make.left.equalTo(wSelf);
        make.top.equalTo(wSelf).offset(2);
        make.bottom.equalTo(wSelf).offset(-2);
    }];
    
    cardView.layer.cornerRadius         = 3;
    cardView.layer.masksToBounds        = NO;
    cardView.backgroundColor            = [UIColor whiteColor];
    cardView.layer.shadowColor          = [UIColor colorWithRed:1.03
                                                 green:1.03
                                                  blue:1.03
                                                 alpha:1.0].CGColor;
    cardView.layer.shadowOffset         = CGSizeZero;
    cardView.layer.shadowOpacity        = 0.8;
    self.cardView                       = cardView;
    self.contentView.backgroundColor    = [UIColor colorWithRed:0.90
                                                       green:0.91
                                                        blue:0.94
                                                       alpha:1.0];;
}

- (void)setupImageAvatar {
    UIImageView *imageView              = [[UIImageView alloc] init];
    self.image                          = imageView;
    [self.contentView addSubview:self.image];
    
    __weak typeof (self) wSelf          = self;
    [self.image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(wSelf);
        make.left.equalTo(wSelf).offset(10);
        make.height.equalTo(@(50));
        make.width.equalTo(@(50));
    }];
    self.image.layer.cornerRadius       = 25;
    self.image.layer.masksToBounds      = YES;
    self.image.backgroundColor          = [UIColor grayColor];
    self.image.contentMode              = UIViewContentModeScaleAspectFill;
    
}
- (void)setupNameLabel {
    __weak typeof (self) wSelf          = self;
    self.fullName                       = [UILabel new];
    [self.contentView addSubview:self.fullName];
    [self.fullName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(20));
        make.left.equalTo(wSelf.image.mas_right).offset(15);
        make.right.equalTo(wSelf).offset(-10);
        make.centerY.equalTo(wSelf.mas_centerY);
    }];
    self.fullName.font = [UIFont systemFontOfSize:18.0];
    self.fullName.textColor = [UIColor blackColor];
    [self.fullName sizeToFit];
}

- (void)setSelected: (BOOL) selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}


@end
