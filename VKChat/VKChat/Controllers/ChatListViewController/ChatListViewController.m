//
//  ChatListViewController.m
//  VKChat
//
//  Created by Valery Efimov on 15.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import "ChatListViewController.h"
#import "ChatViewController.h"
#import "SVPullToRefresh.h"

@interface ChatListViewController ()


@property(strong,nonatomic) AppSessionManager *sessionManager;
@property(assign,nonatomic) BOOL isLoading;
@property(strong,nonatomic) ChatListView *chatListView;
@property(strong,nonatomic)  UIActivityIndicatorView  *barActivityIndicator;

@end

@implementation ChatListViewController


#pragma mark - LifeCycle


- (instancetype)initWithSessionManager:(AppSessionManager *)sessionManager {
    self = [super init];
    
    if (self)
    {
        self.sessionManager                            = sessionManager;
        self.sessionManager.chatListControllerDelegate = self;
        self.dataSource                                = [[ChatsDataSource alloc]initWithSessionmanager:self.sessionManager];
        [self.dataSource setTableDelegate:self];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    [self.chatListView setTableDelegat:self
                          chatDelegate:self
                    chatListDataSource:self.dataSource
                    seacrchBarDelegate:self];
    [self.chatListView.tableView reloadData];
    [self notificationSetup];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)notificationSetup {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addingNewMessage:)
                                                 name:kAddingNewMessage object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userBecomeOnline:)
                                                 name:kFriendBecameOnline object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userBecomeOffline:)
                                                 name:kFriendBecameOffline object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resetMessageFlags:)
                                                 name:kResetTheMessageFlags object:nil];
    
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.chatListView.tableView.delegate        = nil;
    self.chatListView.tableView.dataSource      = nil;
}


#pragma mark - UISetup

- (void)loadView {
    [super loadView];
    
    self.view.backgroundColor                   = [UIColor colorWithRed:0.90 green:0.91 blue:0.94 alpha:1.0];
    self.chatListView                           = [[ChatListView alloc] initWithFrame:self.view.frame];
    self.view                                   = self.chatListView;
    [self.chatListView setNeedsUpdateConstraints];
    [self.chatListView layoutIfNeeded];
    [self navigationBarSetup];
    
}

- (void)navigationBarSetup {
    self.navigationController.navigationBar.barTintColor   = [UIColor colorWithRed:0.22
                                                                           green:0.41
                                                                            blue:0.58
                                                                           alpha:1.0];
    self.navigationController.navigationBar.tintColor      = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent    = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    //settingsButton
    UIBarButtonItem *settingsButton             = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings2@x"]                                                                    style: UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(settingsView:)];
    self.navigationItem.leftBarButtonItem       = settingsButton;
    
    //newChatButton
    UIBarButtonItem *newChatButton              = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"info2@x"]
                                                                             style: UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(newChatView:)];
    //activityIndicator
    UIActivityIndicatorView *activityIndicator  = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.hidesWhenStopped          = YES;
    UIBarButtonItem *itemIndicator              = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    self.navigationItem.rightBarButtonItem      = itemIndicator;
    self.barActivityIndicator                   = activityIndicator;
    
    //spacer
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                          target:self
                                                                          action:nil];
    space.width = 50;
    
    //title
    NSArray *buttons                            = @[newChatButton, space, itemIndicator];
    self.navigationItem.rightBarButtonItems     = buttons;
    self.navigationItem.titleView.tintColor     = [UIColor whiteColor];
    
    self.title = NSLocalizedString(@"Fetching",nil);
    [self.barActivityIndicator startAnimating];
    [self.navigationController.navigationBar setBackgroundImage:nil
                                                  forBarMetrics:UIBarMetricsDefault];
}

- (void)configureNavigationBarState:(NSString *) state {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([state isEqualToString:@"Fetching"])
        {
            self.title = NSLocalizedString(@"Fetching",nil);
            [self.barActivityIndicator startAnimating];
        }
        if ([state isEqualToString:@"Dialogues"])
        {
            self.title = NSLocalizedString(@"Dialogues",nil);
            [self.barActivityIndicator stopAnimating];
        }
    });
}

#pragma mark - NavigationBarActions


- (void)settingsView:(id)sender {
    [self animateGear];
    
}

- (void)newChatView:(id)sender {
    
}


#pragma mark - UIScrollViewDelegate


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.chatListView.searchBar resignFirstResponder];
    self.chatListView.searchBar.showsCancelButton = NO;
    if (scrollView.contentOffset.y <= 0)
    {
            scrollView.contentOffset              = CGPointZero;
    }
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height && self.dataSource.isSearch)
    {
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, scrollView.contentSize.height - scrollView.frame.size.height)];
    }
}


#pragma mark - SVPullToRefresh Actions


- (void)loadMoreChatData {
//    if (self.data > self.dataSource.data.count && !(self.dataSource.isSearch)) {
//        [self.sessionManager.vkManager getDialogsWithOffset:[NSString stringWithFormat:@"%lu",(unsigned long)self.dataSource.data.count]
//                                                        count:@"10"
//                                               startMessageID:@""
//                                                  needsUnread:@"1"
//                                               needsImportant:@"0"
//                                              needsUnAnswered:@"0"
//                                                   completion:^(NSArray *items, NSInteger count, NSInteger unreadCount) {
//            self.tableView.showsInfiniteScrolling = YES;
//            [self chatTableSetupAfterDownloadingWithArray:items count:count unreadedCount:unreadCount];
//        }];
//    }
//    else
//    {
//        self.tableView.showsInfiniteScrolling = NO;
//    }
}


#pragma mark - ILRemoteSearchBarDelegate


- (void)remoteSearchBar:(ILRemoteSearchBar *)searchBar
          textDidChange:(NSString *)searchText {
    [searchBar setShowsCancelButton:YES animated:YES];
    if(searchText.length < 1)
    {
        _dataSource.isSearch = NO;
        [self.chatListView.tableView reloadData];
    }
    else
    {
        [self.sessionManager.vkManager messagesSearchDialogsWithQ:searchText limit:@"20" completion:^(NSArray *items) {
            [self.dataSource configureSearchDataWithArray:items];
            [self.chatListView.tableView reloadData];
            
        }];
    }
}


#pragma mark - TableDelegateProtocol


- (void)reloadRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths
              withRowAnimation:(UITableViewRowAnimation)animation  {
    [self.chatListView.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (UITableViewCell *)getCellForRowAtIndexPath:(NSIndexPath *)indexPat {
    return [self.chatListView.tableView cellForRowAtIndexPath:indexPat];
}

- (void)reloadTable {
    dispatch_sync(dispatch_get_main_queue(), ^{
       [self.chatListView.tableView reloadData];
    });
}

- (void)beginUpdates {
    [self.chatListView.tableView beginUpdates];
}

- (void)endUpdates {
    [self.chatListView.tableView endUpdates];
}

- (void)insertSections:(NSIndexSet *)sections withRowAnimation:(UITableViewRowAnimation)animation {
    [self.chatListView.tableView insertSections:sections withRowAnimation:UITableViewRowAnimationNone];
}

- (void)deleteSections:(NSIndexSet *)sections withRowAnimation:(UITableViewRowAnimation)animation {
    [self.chatListView.tableView deleteSections:sections withRowAnimation:UITableViewRowAnimationNone];
}

- (void)insertRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation {
    [self.chatListView.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (void)deleteRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation {
    [self.chatListView.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}



#pragma mark - UISearchBarDelegate


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    self.dataSource.isSearch        = NO;
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text                  = @"";
    [self.dataSource.searchData removeAllObjects];
    [self.chatListView.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
    if(searchBar.text.length < 1)
    {
        self.dataSource.isSearch = NO;
    }
    else
    {
        [self.sessionManager.vkManager messagesSearchDialogsWithQ:searchBar.text
                                                            limit:@"20"
                                                       completion:^(NSArray *items) {
            [self.dataSource configureSearchDataWithArray:items];
        }];
    }
}


#pragma mark - Animations


- (void)animateGear {
    UIView *itemView = [self.navigationItem.leftBarButtonItem performSelector:@selector(view)];
    UIImageView *imageView = [itemView.subviews firstObject];
    imageView.rotate(360).easeInOutBack.animate(0.4);
}



#pragma mark - UITableViewDelegate


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [BaseTableViewCell cellHeigth];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.chatListView.searchBar resignFirstResponder];
    ChatDBModel * model;
    //    if (self.dataSource.isSearch)
    //    {
    //        SearchChatsModel *sModel  = [self.dataSource.searchData objectAtIndex:indexPath.row];
    //        model = [self searchModelIntoChatModel:sModel];
    //        // create new single user chat
    //    }
    //    else
    //    {
    model = self.dataSource.fetchedResultsController.fetchedObjects[indexPath.row];
    //    }
    ChatViewController *chatViewController = [[ChatViewController alloc] initWithChatModel:model sessionManager:self.sessionManager];
    
    NSString * peerID = @"";
    if (model.isP2P)
    {
        NSInteger peer = kPeer + [model.chatID integerValue];
        peerID = [NSString stringWithFormat:@"%ld",(long)peer];
    }
    [VKManager getHistoryWithOffset:@"0"
                              count:@"50"
                             userId:model.userID
                             peerId:peerID
                     startMessageId:@""
                                rev:@""
                         completion:^(NSDictionary *item) {
                             
        model.messagesMaxCount          = [item[kCountChats] stringValue];
        NSManagedObjectContext *context = model.managedObjectContext;
        
        NSArray *arrayItems             = [NSArray arrayWithArray:item[kItems] ];
        arrayItems                      = [[arrayItems reverseObjectEnumerator]allObjects];
        
        for (NSInteger i = 0; i < arrayItems.count; i++)
        {
            MessageDBModel *message     = [[model.messages filteredOrderedSetUsingPredicate:[NSPredicate predicateWithFormat:@"messageID == %@",[arrayItems[i][kMessageId] stringValue]]] firstObject];
            if(!message)
            {
                message                 = [MessageDBModel newEntityWithContext:context];
                [message setupWithJson:arrayItems[i] context:context];
                [model addMessagesObject:message];
            }
        }
        
        [self.navigationController pushViewController:chatViewController animated:YES];
    }];
}

//- (ChatModel*)searchModelIntoChatModel:(SearchChatsModel*)searchModel {
//   
//    ChatModel *model;
//    if (!searchModel.chatID)
//    {
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID == %@", searchModel.userID ];
//        NSArray *filteredArray = [self.dataSource.data filteredArrayUsingPredicate:predicate];
//        model = [filteredArray firstObject];
//
//    }
//    else
//    {
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"chatID == %@", searchModel.chatID ];
//        NSArray *filteredArray = [self.dataSource.data filteredArrayUsingPredicate:predicate];
//        model = [filteredArray firstObject];
//    }
//    
//    return model;
//}


#pragma mark - Notifacations actions


- (void)addingNewMessage:(NSNotification *)notification {
    ChatDBModel *model;
    
    //fault
    NSString *fields                            = @"photo_50, photo_100, photo_200_orig, online,  last_seen";
    __block NSManagedObjectContext *context     = [[DataBaseManager sharedInstance] childManagedObjectContext];
   
    if([[notification object][3] integerValue] < kPeer)
    {
        NSString *userId                        = [NSString stringWithFormat:@"%@", [notification object][3]];
        model = [[DataBaseManager sharedInstance] messagesGetChatWithChatId: userId context:context];
        if(!model)
        {
            model = [ChatDBModel newEntityWithContext:context];
            [VKManager messagesGetDialogWithUserId:userId
                                            fields:fields
                                        completion:^(NSDictionary *json) {
                [model setupWithJson:json completion:^{
                [model setupDialogWithNotification:notification
                                           context:context
                                            chatId:userId];
                }];
            }];
        }
        else
        {
            [model setupDialogWithNotification:notification context:context chatId:userId];
        }
    }
    else
    {
        NSInteger i                             = [[NSString stringWithFormat:@"%@",[notification object][3] ] integerValue]  - kPeer;
        NSString *chatId                        = [NSString stringWithFormat:@"%ld", (long)i];
        model                                   = [[DataBaseManager sharedInstance] messagesGetChatWithChatId:chatId
                                                                                                      context:context];
        if(!model)
        {
            model = [ChatDBModel newEntityWithContext:context];
            [VKManager messagesGetChatWithChatId:chatId
                                          fields:fields
                                      completion:^(NSDictionary *json) {
                [model setupDialogWithNotification:notification
                                           context:context
                                            chatId:model.chatID];
                [model setupWithJson:json completion:^{}];
               
            }];
        }
        else
        {
            [model setupDialogWithNotification:notification
                                       context:context
                                        chatId:model.chatID];
        }
        
    }
    [[DataBaseManager sharedInstance] saveContext:context];
}




#pragma mark -


- (void)userBecomeOnline:(NSNotification *)notification {
    [self resetUserStateWithState:@"1" notification:notification];
}

- (void)userBecomeOffline:(NSNotification *)notification {
    [self resetUserStateWithState:@"0" notification:notification];
}

- (void)resetUserStateWithState:(NSString *) state notification:(NSNotification *)notification {
    ChatDBModel *model;
    UserDBModel *user;
    NSManagedObjectContext *context = [[DataBaseManager sharedInstance] childManagedObjectContext];
    if([[notification object][1]longValue]  < kPeer)
    {
        NSString *userId     = [NSString stringWithFormat:@"%@", [notification object][1]];
        userId               = [userId stringByReplacingOccurrencesOfString:@"-" withString:@""];
        model                = [[DataBaseManager sharedInstance] messagesGetChatWithChatId:userId context:context];
        if(model)
        {
            user             = [model.users array][model.users.count - 1];
            user.online      = state;
            model.userOnline = user.online;
        }
    }
    else
    {
        NSInteger i          = [[notification object][1] longValue]  + kPeer;
        NSString *chatId     = [NSString stringWithFormat:@"%ld", i];
        chatId               = [chatId stringByReplacingOccurrencesOfString:@"-" withString:@""];
        model                = [[DataBaseManager sharedInstance] messagesGetChatWithChatId:chatId
                                                                                   context:context];
        if(model)
        {
            user             = [model.users array][model.users.count - 1];
            user.online      = state;
            model.userOnline = user.online;
        }
    }
    [[DataBaseManager sharedInstance] saveContext:context];
}

- (void)resetMessageFlags:(NSNotification *)notification {
//    NSArray *object = [[NSArray alloc] initWithArray:[notification object]];
//    ChatModel *model = [ChatModel new];
//    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lastMessageID == %@",[object[1] stringValue] ];
//    NSArray *filteredArray = [self.dataSource.data filteredArrayUsingPredicate:predicate];
//    NSInteger anIndex = [self.dataSource.data indexOfObject:[filteredArray firstObject]];
//    model = [filteredArray firstObject];
//    if(model)
//    {
//        [self.dataSource.data removeObject:model];
//        model.readState = @"1";
//        model.unreadedCount = 0;
//        [self.dataSource.data insertObject:model atIndex:anIndex];
//    }
//    [self reloadData];

    MessageDBModel *message;
    NSManagedObjectContext *context = [[DataBaseManager sharedInstance] childManagedObjectContext];

        NSString *messageId         = [NSString stringWithFormat:@"%@", [notification object][1]];
        message                     = [[DataBaseManager sharedInstance] messagesGetWithMessageId:messageId context:context];
        if(message)
        {
            message.readState       = @"1";
            //message.unreadedCount = 0;
        }
    [[DataBaseManager sharedInstance] saveContext:context];
}

@end
