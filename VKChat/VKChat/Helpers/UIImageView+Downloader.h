//
//  UIImageView+Downloader.h
//  QuamTest
//
//  Created by Valery Efimov on 29.11.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Downloader)


+ (void)loadImageWithURL:(NSURL*)URL WithCompletion:(void (^)(UIImage * image))completionBlock;


@end
