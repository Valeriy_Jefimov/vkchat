//
//  NSDate+Unixtime.m
//  VKChat
//
//  Created by Valery Efimov on 15.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import "NSDate+Unixtime.h"

@implementation NSDate (Unixtime)


+ (NSDate *)dateWithUnixtime:(NSInteger) integer {
    return [NSDate dateWithTimeIntervalSince1970:integer];
}


@end
