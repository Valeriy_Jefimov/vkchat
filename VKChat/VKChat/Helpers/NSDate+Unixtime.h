//
//  NSDate+Unixtime.h
//  VKChat
//
//  Created by Valery Efimov on 15.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Unixtime)

+ (NSDate *)dateWithUnixtime:(NSInteger) integer;


@end
