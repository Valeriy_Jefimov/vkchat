//
//  BaseTableViewCell.h
//  VKChat
//
//  Created by Valery Efimov on 10.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell


- (id)initWithStyle: (UITableViewCellStyle) style
      reuseIndefier: (NSString *) indefier;
+ (CGFloat)cellHeigth;
+ (CGSize)getSizeForText:(NSString *)text
                maxWidth:(CGFloat)width
                    font:(NSString *)fontName
                fontSize:(float)fontSize;

@end
