//
//  BaseTableViewCell.m
//  VKChat
//
//  Created by Valery Efimov on 10.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (id)initWithStyle: (UITableViewCellStyle) style
      reuseIndefier: (NSString *) indefier {
    self = [super initWithStyle:style reuseIdentifier:indefier];
    
    return self;
}

+ (CGFloat)cellHeigth {
    return 80.0;
}

+ (CGSize)getSizeForText:(NSString *)text
                maxWidth:(CGFloat)width
                    font:(NSString *)fontName
                fontSize:(float)fontSize {
    CGSize constraintSize;
    constraintSize.height = MAXFLOAT;
    constraintSize.width = width;
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:fontName size:fontSize], NSFontAttributeName,
                                          nil];
    
    CGRect frame = [text boundingRectWithSize:constraintSize
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributesDictionary
                                      context:nil];
    
    CGSize stringSize = frame.size;
    return stringSize;
}

@end
