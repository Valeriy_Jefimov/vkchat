//
//  BaseTableViewDataSourse.m
//  VKChat
//
//  Created by Valery Efimov on 04.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import "BaseTableViewDataSource.h"

@interface BaseTableViewDataSource ()


@property (strong,nonatomic) id<TableDelegateProtocol> delegate;
@property (strong,nonatomic) NSCache *cashe;

@end


@implementation BaseTableViewDataSource

- (instancetype)initWithJson:(NSArray *)json sessionManager:(AppSessionManager *)sessionManager{
    
    self = [super init];
    
    if (self)
    {
        self.sessionManager = sessionManager;
        self.data = [NSMutableArray new];
        self.data = json.mutableCopy;
        self.cashe = [NSCache new];
    }
    return self;
}

- (void)setTableDelegat: (id) aDelegate {
    self.delegate = aDelegate;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger) section {

   return self.data.count;
}

- (NSInteger)numberOfSectionsInTableView: (UITableView *) tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
  
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:NSStringFromClass([UITableViewCell class])];
        }
        return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {

    return NO;
}

- (void)  tableView:(UITableView *)tableView
 commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
  forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
    }
}

- (NSInteger)dataCount {

    return self.data.count;
}

@end


