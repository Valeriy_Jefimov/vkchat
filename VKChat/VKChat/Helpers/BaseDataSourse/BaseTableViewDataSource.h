//
//  BaseTableViewDataSourse.h
//  VKChat
//
//  Created by Valery Efimov on 04.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppSessionManager.h"

@protocol TableDelegateProtocol <NSObject>

- (UITableViewCell *)getCellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)reloadTable;
- (void)beginUpdates;
- (void)endUpdates;
- (void)insertSections:(NSIndexSet *)sections withRowAnimation:(UITableViewRowAnimation)animation;
- (void)deleteSections:(NSIndexSet *)sections withRowAnimation:(UITableViewRowAnimation)animation;
- (void)insertRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation;
- (void)deleteRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation;
- (void)reloadRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation;

@end

@interface BaseTableViewDataSource : NSObject <UITableViewDataSource>

- (instancetype)initWithJson:(NSArray *)json sessionManager:(AppSessionManager *)sessionManager;

- (void)setTableDelegat: (id) aDelegate;
- (NSInteger)dataCount;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger) section;
- (NSInteger)numberOfSectionsInTableView: (UITableView *) tableView;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath ;
- (void)  tableView:(UITableView *)tableView
 commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
  forRowAtIndexPath:(NSIndexPath *)indexPath;

@property (strong,atomic) NSMutableArray *data;
@property (assign,atomic) NSInteger dataMaxCount;
@property (strong,nonatomic) AppSessionManager *sessionManager;

@end
