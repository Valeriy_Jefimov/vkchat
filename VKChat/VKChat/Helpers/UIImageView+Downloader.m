//
//  UIImageView+Downloader.m
//  QuamTest
//
//  Created by Valery Efimov on 29.11.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#import "UIImageView+Downloader.h"
#import "VKManager.h"


@implementation UIImageView (Downloader)


+ (void)loadImageWithURL:(NSURL*) URL WithCompletion:(void (^)(UIImage * image))completionBlock{
        dispatch_queue_t queue = dispatch_queue_create("photoList", NULL);
        dispatch_async(queue, ^{
            [VKManager imageWithURL:URL success:^(UIImage *image) {
                
                completionBlock(image);
                
            } failure:^(NSError *error) {
              
                    completionBlock([UIImage imageNamed:@"ava"]);

            }];
        });
}


@end
