//
//  Header.h
//  VKChat
//
//  Created by Valery Efimov on 10.01.17.
//  Copyright © 2017 Valery Efimov. All rights reserved.
//

#ifndef Header_h
#define Header_h
//scope
static NSString * const kScope =                        @"scope";
static NSString * const kImageScope =                   @"photos";
static NSString * const kOfflineScope =                 @"offline";
static NSString * const kMessagesScope =                @"messages";
static NSString * const kAudioScope =                   @"audio";
static NSString * const kVideoScope =                   @"video";
static NSString * const kFriendsScope =                 @"friends";


//login
static NSString * const kClientId =                     @"client_id";
static NSString * const kAppId =                        @"5774870";
static NSString * const kDisplay =                      @"display";
static NSString * const kMobile =                       @"mobile";
static NSString * const kRedirectUri =                  @"redirect_uri";
static NSString * const kRedirectUriEndPoint =          @"https://oauth.vk.com/blank.html";
static NSString * const kResponseType =                 @"response_type";
static NSString * const kResponseTypeEndPoint =         @"token";
static NSString * const kVersion =                      @"v";
static NSString * const kVersionEndPoint =              @"5.60";
static NSString * const kOAuthEndPoint =                @"https://oauth.vk.com/authorize?";

//types
static NSString * const kPostType =                     @"POST";
static NSString * const kGetType =                      @"GET";
static NSString * const kType =                         @"type";
static NSString * const kServise =                      @"UserSession";
static NSString * const kSessionToken =                 @"access_token";
static NSString * const kOwnerId =                      @"owner_id";
static NSString * const kVKEndpoint =                   @"https://api.vk.com/method/";

//friends
static NSString * const kUsersID =                      @"user_id";
static NSString * const kUsers =                        @"users";
static NSString * const kOrder =                        @"order";
static NSString * const kFriendsFieldsParametr =        @"fields";
static NSString * const kFriendsFields =                @"photo_100, online, last_seen";
static NSString * const kNameCaseParametr =             @"name_case";
static NSString * const kNameCase =                     @"nom";

//createChat
static NSString * const kUsersIDs =                     @"user_ids";
static NSString * const kTitle =                        @"title";

//get chats
static NSString * const kUnreadParametr =               @"unread";
static NSString * const kChatId =                       @"id";
static NSString * const kStartMessageIdParametr =       @"start_message_id";
static NSString * const kPreviewLengthParametr =        @"preview_length";
static NSString * const kCountChatsParametr =           @"count";
static NSString * const kResponseParametr =             @"response";
static NSString * const kOffsetParametr =               @"offset";
static NSString * const kImportantParametr =            @"important";
static NSString * const kUnasweredParametr =            @"unanswered";
static NSString * const kMessageIdsParametr =           @"message_ids";


static NSString * const kItems =                        @"items";
static NSString * const kPeerId =                       @"peer_id";
static NSString * const kCountChats =                   @"count";
static NSString * const kUnread =                       @"unread";
static NSString * const kMessage =                      @"message";
static NSString * const kInRead =                       @"in_read ";
static NSString * const kOutRead =                      @"out_read";
static NSString * const kUnreadDialogs =                @"unread_dialogs";
static NSString * const kUploadUrl =                    @"upload_url";
static NSString * const kLimit =                        @"limit";
static NSString * const kFields =                       @"fields";

//methods

static NSString * const kGetDialogsMethod =             @"messages.getDialogs";
static NSString * const kCreateChatMethod =             @"messages.createChat";
static NSString * const kRegistreDeviceMethod =         @"account.registerDevice";
static NSString * const kSetOfflineMethod =             @"account.setOffline";
static NSString * const kSetOnlineMethod =              @"account.setOnline";
static NSString * const kSetPushSettingsMethod =        @"account.setPushSettings";
static NSString * const kSetSilenceModeMethod =         @"account.setSilenceMode";
static NSString * const kUnregisterDeviceMethod =       @"account.unregisterDevice";
static NSString * const kAudioGetMethod =               @"audio.get";
static NSString * const kAudioGetByIdMethod =           @"audio.getById";
static NSString * const kDocsAddMethod =                @"docs.add";
static NSString * const kDocsGetWithCountMethod =       @"docs.get";
static NSString * const kFriendsGetWithQMethod =        @"friends.search";
static NSString * const kMessagesDeleteDialog =         @"messages.deleteDialog";
static NSString * const kMessagesSearchDialogs =        @"messages.searchDialogs";
static NSString * const kMessagesGetHistory =           @"messages.getHistory";
static NSString * const kGetChatMethod =                @"messages.getChat";
static NSString * const kGetMessageByIdMethod =         @"messages.getById";

//users

static NSString * const kFirstName =                    @"first_name";
static NSString * const kLastName =                     @"last_name";
static NSString * const kUserId =                       @"id";
static NSString * const kPhoto50Url =                   @"photo_50";
static NSString * const kPhoto100Url =                  @"photo_100";
static NSString * const kPhoto200Url =                  @"photo_200";


//message

static NSString * const kFromID =                       @"from_id";
static NSString * const kDate =                         @"date";
static NSString * const kUserID =                       @"user_id";
static NSString * const kMessageId =                    @"id";
static NSString * const kReadState =                    @"read_state";
static NSString * const kMessageOut =                   @"out";
static NSString * const kBody =                         @"body";
static NSString * const kGeo =                          @"geo";
static NSString * const kAttachments =                  @"attachments";
static NSString * const kForwardMessages =              @"fwd_messages";
static NSString * const kEmojiState =                   @"emoji";
static NSString * const kDeleted =                      @"deleted";
static NSString * const kRandomId =                     @"random_id";
static NSString * const kChatActive =                   @"chat_active";
static NSString * const kPushSettings =                 @"push_settings";
static NSString * const kUsersCount =                   @"users_count";
static NSString * const kAdminId =                      @"admin_id";
static NSString * const kAction =                       @"action";
static NSString * const kActionMid =                    @"action_mid";
static NSString * const kActionEmail =                  @"action_email";
static NSString * const kActionText =                   @"action_text";
static NSString * const kMessageChatID =                @"chat_id";
static NSString * const kOnline =                       @"online";
static NSString * const kLastSeen =                      @"last_seen";
static NSString * const kTime =                         @"time";
static NSString * const kPlatform =                     @"platform";

static NSInteger  const kPeer =                         2000000000;

#endif /* Header_h */
