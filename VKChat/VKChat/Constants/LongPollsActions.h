//
//  LongPollsActions.h
//  VKChat
//
//  Created by Valery Efimov on 14.12.16.
//  Copyright © 2016 Valery Efimov. All rights reserved.
//

#ifndef LongPollsActions_h
#define LongPollsActions_h

static NSString * const kReplacementOfMessageFlags =                    @"replacementOfMessageFlags";
static NSString * const kSettingTheMessageFlags =                       @"settingTheMessageFlags";
static NSString * const kResetTheMessageFlags =                         @"resetTheMessageFlags";
static NSString * const kAddingNewMessage =                             @"addingNewMessage";
static NSString * const kReadingAllIncomingMessages =                   @"readingAllIncomingMessages";
static NSString * const kReadingAllOutboundMessages =                   @"readingAllOutboundMessages";
static NSString * const kFriendBecameOnline =                           @"friendBecameOnline";
static NSString * const kFriendBecameOffline =                          @"friendBecameOffline";
static NSString * const kResetDialogFlags =                             @"resetDialogFlags";
static NSString * const kReplacementDialogFlags =                       @"replacementDialogFlags";
static NSString * const kSettingDialogFlags =                           @"settingDialogFlags";
static NSString * const kParametersOfCompositionOrThemeHaveChanged =    @"parametersOfCompositionOrThemeHaveChanged";
static NSString * const kUserStartedTypingInDialog =                    @"userStartedTypingInDialog";
static NSString * const kUserStartedTypingInChat=                       @"userStartedTypingInChat";
static NSString * const kUserMakesACall =                               @"userMakesACall";
static NSString * const kUpdateUnreadCount =                            @"updateUnreadCount";
static NSString * const kNotificationSettingsHaveChanged =              @"notificationSettingsHaveChanged";

#endif /* LongPollsActions_h */
